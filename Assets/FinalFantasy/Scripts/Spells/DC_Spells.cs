﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//created by Dominic Christiano
public class DC_Spells : MonoBehaviour
{
    public GameObject[] Magic = new GameObject[64];

    public GameObject[] whiteMagic = new GameObject[32];
    public GameObject[] blackMagic = new GameObject[32];
    public DC_SpellStats ST;
     
    // Organizes all spells into Array to be indexed
    void SpellOrganizer()
    {
        Magic[0] = whiteMagic[0];  
        Magic[1] = whiteMagic[0];
        Magic[2] = whiteMagic[0];
        Magic[3] = whiteMagic[0];
        Magic[4] = blackMagic[0];    
        Magic[5] = blackMagic[0];
        Magic[6] = blackMagic[0];
        Magic[7] = blackMagic[0];

    }
}