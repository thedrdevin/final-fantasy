﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/*-------------------------------------------------------------------------------------------------------------------------------
* Name: Dongwon Yoo
 * Date: 1/1/16
 * Credit: Final Fantasy 1
 * Purpose: This Script is getting the spell's information from the SpellData.txt and SpellData.cs files.
 --------------------------------------------------------------------------------------------------------------------------*/

public class SpellIdentifier : MonoBehaviour
{
    [SerializeField] private int spellID;
    [SerializeField] private int spellNum;
    [SerializeField] private string spellName;
    [SerializeField] private int spellEfctvty;
    [SerializeField] private int acc;
    [SerializeField] private string element;
    [SerializeField] private string effect;

    // Use this for initialization
    void Start()
    {
        //MonsterData monster = new MonsterData();
        //Struct_MonsterData data = monster.monsterData[spellID];
        //Debug.Log(spellEfctvty);
        Struct_SpellData data = SpellData.ReadData("./Assets/SpellData.txt", spellID);

        spellNum = data.num;
        spellName = data.name;
        spellEfctvty = data.efctvty;
        acc = data.acc;
        element = data.Element;
        effect = data.effect;

    }

    // Update is called once per frame
    void Update()
    {

    }

    public int SpellIdentity
    {
        get
        {
            return spellID;
        }
        set
        {
            spellID = value;
        }
    }
}
