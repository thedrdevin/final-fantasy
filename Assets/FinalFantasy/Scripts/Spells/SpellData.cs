﻿using System.IO;
using System.Collections.Generic;

/*-------------------------------------------------------------------------------------------------------------------------------
* Name: Dongwon Yoo
 * Date: 1/1/16
 * Credit: Final Fantasy 1
 * Purpose: This Script is for reading the SpellData.txt file.
 --------------------------------------------------------------------------------------------------------------------------*/

public class Struct_SpellData
{
    public int num;
    public string name;
    public int efctvty;
    public int acc;
    public string Element;
    public string effect;
}

public class SpellData
{
    public List<Struct_SpellData> spellData;

    public SpellData()
    {
        SpellData spellFileRead = new SpellData();
        spellData = ReadData("./Assets/SpellData.txt");
    }

    public List<Struct_SpellData> ReadData(string filepath)
    {
        List<Struct_SpellData> result = new List<Struct_SpellData>();
        using (StreamReader sr = File.OpenText(filepath))
        {
            string s = "";
            while ((s = sr.ReadLine()) != null)
            {
                string[] strs;
                strs = s.Split('/');
                Struct_SpellData newSpell = new Struct_SpellData();
                newSpell.num = int.Parse(strs[0]);
                newSpell.name = strs[1];
                newSpell.efctvty = int.Parse(strs[2]);
                newSpell.acc = int.Parse(strs[3]);
                newSpell.Element = strs[4];
                newSpell.effect = strs[5];
                result.Add(newSpell);
            }
        }

        return result;
    }

    public static Struct_SpellData ReadData(string filepath, int spellType)
    {
        using (StreamReader sr = File.OpenText(filepath))
        {
            string s = "";
            for (int i = 0; (s = sr.ReadLine()) != null; i++)
            {
                if (i != spellType)
                    continue;

                string[] strs;
                strs = s.Split('/');
                Struct_SpellData newSpell = new Struct_SpellData();
                newSpell.num = int.Parse(strs[0]);
                newSpell.name = strs[1];
                newSpell.efctvty = int.Parse(strs[2]);
                newSpell.acc = int.Parse(strs[3]);
                newSpell.Element = strs[4];
                newSpell.effect = strs[5];

                return newSpell;
            }
        }

        return new Struct_SpellData();
    }
}
