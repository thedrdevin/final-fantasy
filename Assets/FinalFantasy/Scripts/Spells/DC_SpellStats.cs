﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//created by Dominic Christiano
public class DC_SpellStats : MonoBehaviour {

    int ATK;
    float HitPercent;
    int CritChance;
    int Price;
    public GameObject[] Spell;
    float Evasion;
    int Defense;
    public bool stunned;
    

    // Use this for initialization
    void Start()
    {
        Spell[0] = GameObject.Find("Cure");
        Spell[1] = GameObject.Find("Harm");
        Spell[2] = GameObject.Find("Fog");
        Spell[3] = GameObject.Find("Ruse");
        Spell[4] = GameObject.Find("Fire");
        Spell[5] = GameObject.Find("Slep");
        Spell[6] = GameObject.Find("Lock");
        Spell[6] = GameObject.Find("Lit");
    }

    public void SetCure()
    {
        ATK = Random.Range(-16, -32);
        HitPercent = 1f;
        CritChance = 0;
        Price = 100;
    }
    public void SetRuse()
    {
        ATK = 0;
        HitPercent = 0.4f;
        Price = 100;
        Evasion = 0.8f;
    }
    public void SetFog()
    {
        ATK = 9;
        HitPercent = 0.05f;
        Price = 100;
        //"Put in current defense when made here  +"
        Defense = 8;
    }
    public void SetFire()
    {
        ATK = Random.Range(10, 40);
        HitPercent = 0.7f;
        CritChance = 0;
        Price = 100;
    }
    public void SetSlep()
    {
        ATK = 0;
        HitPercent = 0.05f;
        CritChance = 0;
        Price = 100;
        stunned = true;
    }

    public void SetLock()
    {
        ATK = 0;
        HitPercent = 0.05f;
        CritChance = 0;
        //add the negative 8 to current evasiveness
        Evasion = -8;
        Price = 100;

    }

    public void SetLit()
    {
        ATK = Random.Range(10, 40);
        HitPercent = 0.05f;
        CritChance = 1;
        Price = 100;

    }
}
