﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class L_Camera : MonoBehaviour {

    public Transform TargetToFollow;
    public Transform selfTrans;
    public Vector3 offset = new Vector3(0f, 0f, 0f);

    private void LateUpdate()
    {
        selfTrans.position = new Vector3(TargetToFollow.position.x, TargetToFollow.position.y, 0) + offset;
    }


}
