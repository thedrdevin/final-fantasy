﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//made by Dominic Christiano the script is to set the doors and ceilings so that the correct visual is seen
public class DC_DoorsAndCeilings : MonoBehaviour {
    [SerializeField]
    public GameObject door;  
    [SerializeField]
    public GameObject ceiling;
    [SerializeField]
    public GameObject player;

    


	// Use this for initialization
	void Start () {
       
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    //when the player enters the trigger the door opened sprite pops up and the ceiling disappers 
    void OnTriggerStay2D(Collider2D inside)
    {
        if (inside.gameObject.tag == "Player")
        {
            door.SetActive(true);
            ceiling.SetActive(false);   
        }
       
    }

    //when the player leaves the door disappears and the ceiling returns
    void OnTriggerExit2D(Collider2D leaving)
    {
        if(leaving.gameObject.tag  == "Player")
        {
            door.SetActive(false);
            ceiling.SetActive(true);
        }
    }

}
