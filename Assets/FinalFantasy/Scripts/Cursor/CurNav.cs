﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

//Nicholas Chaparro
//

public class CurNav : MonoBehaviour
{

    private CurMove cursor; //reference to PM_Cursor script to get tranforms for the cursor and access to the reset timer script
    

    

    
    

    enum STATES //states for the state machine 
    {
        Continue,
        NewGame

    }
    STATES curState = STATES.Continue; //sets current state on load
    Dictionary<STATES, Action> stateRegistry = new Dictionary<STATES, Action>(); //allows states to be set inside the dictionary

    // Use this for initialization

    void Start()
    {
        //Status = GameObject.Find("StatusBlockTemplate");
        cursor = GameObject.Find("Cursor").GetComponent<CurMove>(); //finds the game object cursor 2 and sets it as "cursor" for local reference

        stateRegistry.Add(STATES.Continue, new Action(ContinueState));
        stateRegistry.Add(STATES.NewGame, new Action(NewGameState));
    }

    // Update is called once per frame
    void Update()
    {
        //allows the states to exist during play and activates functions within that state
        stateRegistry[curState].Invoke();

        if (curState == STATES.Continue)
        {
            ContinueState();
        }
        else if(curState == STATES.NewGame)
        {
            NewGameState();
        }
    }

    void SetState(STATES newState) //sets states 
    {
        curState = newState;
    }

    void ContinueState()
    {
        if (cursor.TimerCap <= cursor.TimerTracked)
        {
            cursor.ContinueChosen();
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                cursor.NewGameChosen();
                SetState(STATES.NewGame);
                cursor.ResetTimer();

            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                cursor.NewGameChosen();
                SetState(STATES.NewGame);
                cursor.ResetTimer();

            }

            else if (Input.GetKeyDown(KeyCode.Z))
            {
                SceneManager.LoadScene(1);
            }
        
        }

    }
    void NewGameState()
    {
         cursor.NewGameChosen();
        if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                cursor.NewGameChosen();
                SetState(STATES.NewGame);
                cursor.ResetTimer();

            }
           if (Input.GetKey(KeyCode.UpArrow))
            
            {
            Debug.Log("curMove");
            cursor.NewGameChosen();
                SetState(STATES.NewGame);
                cursor.ResetTimer();
            
        }
    }
}    

