﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Code by Nicholas Chaparro
//This code handles the cursor movement in the main menu
//2-21-2018

public class CursorMovement : MonoBehaviour
{
    public float curSpeed;
    private Transform tf;
    [SerializeField] private float timerReachPoint;
    private float timerTracker;
    SpriteRenderer spr;

	// Use this for initialization
	void Start ()
    {
        
        tf = GetComponent<Transform>();
        spr = GetComponent<SpriteRenderer>();
        ResetTimer();
       curSpeed = 4f;	
	}
	
	// Update is called once per frame
	void Update ()
        
    {
        timerTracker += Time.deltaTime;
        transform.Translate(0, curSpeed * Input.GetAxis("Vertical"), 0f);
    }
    public void ResetTimer()
    {
        
        timerTracker = 0;
    }
    public void Dissapearing()
    {
        spr.sprite = Resources.Load("None", typeof(Sprite)) as Sprite;
    }
    public void Appearing()
    {
        spr.sprite = Resources.Load("Cursor", typeof(Sprite)) as Sprite;
    }
    public void ContinueChosen()
    {
        Debug.Log("SomethingHere");
        tf.position = new Vector2(-1.3f, -2.1f);
    }
    public void NewGameChosen()
    {
        tf.position = new Vector2(-1.3f, -2.8f);
    }
    public float TimerReach
    {
        get
        {
            return timerReachPoint;
        }
    }
    public float TimerTracker
    {
        get
        {
            return timerTracker;
        }
    }
    void OnTriggerStay2D(Collider2D collision)
    {

        if (Input.GetKeyDown(KeyCode.D))

        {
            SceneManager.LoadScene(1);
        }
    }
}

