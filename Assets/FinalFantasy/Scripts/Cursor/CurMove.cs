﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Code by Nicholas Chaparro

public class CurMove : MonoBehaviour {

    [SerializeField]
    private Transform PM_C;
    [SerializeField]
    private float TimerCapHit;
    private float TimerTracking;

    // Use this for initialization
    void Start()
    { //calls the transform and sets 
        PM_C.GetComponent<Transform>();
        ResetTimer();
    }

    // Update is called once per frame
    void Update()
    {
        TimerTracking += Time.deltaTime;
    }
    //values for the transform of the cursor inside the pause menu
    //also a way to reset the timer to keep from having stack overflow errors
    public void ResetTimer()
    {
        TimerTracking = 0;
    }
    public void ContinueChosen()
    {
        PM_C.position = new Vector2(350.0f, 350.0f);
    }
    public void NewGameChosen()
    {
        PM_C.position = new Vector2(350.0f, 230.0f);
    }
    public float TimerCap
    {
        get
        {
            return TimerCapHit;
        }
    }

    public float TimerTracked
    {
        get
        {
            return TimerTracking;
        }
    }

}
