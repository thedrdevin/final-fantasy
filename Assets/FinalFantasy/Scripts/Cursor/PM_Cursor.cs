﻿/*-------------------------------------------------------------------------------------------------------------------------------
* Name: Kyle Harrington
 * Date: 1/17/2018
 * Credit: Project : FinalFantasy 1 Nes Remake
 * Purpose: This script stores the transform of the cursor for each state within the pause menu as well as a ResetTimer function used in many of the other scripts
 * 
 --------------------------------------------------------------------------------------------------------------------------*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PM_Cursor : MonoBehaviour {

    [SerializeField]
    private Transform PM_C;
    [SerializeField]
    private float TimerCapHit;
    private float TimerTracking;

	// Use this for initialization
	void Start () { //calls the transform and sets 
        PM_C.GetComponent<Transform>();
        ResetTimer();
	}
	
	// Update is called once per frame
	void Update ()
    {
        TimerTracking += Time.deltaTime;
	}
    //values for the transform of the cursor inside the pause menu
    //also a way to reset the timer to keep from having stack overflow errors
    public void ResetTimer()
    {
        TimerTracking = 0;
    }
    public void ItemChosen()
    {
        PM_C.position = new Vector2(-6.8f, -1.2f);
    }
    public void MagicChosen()
    {
        PM_C.position = new Vector2(-6.8f, -2.2f);
    }
    public void WeaponChosen()
    {
        PM_C.position = new Vector2(-6.8f, -3.2f);
    }
    public void ArmorChosen()
    {
        PM_C.position = new Vector2(-6.8f, -4.2f);
    }
    public void StatusChosen()
    {
        PM_C.position = new Vector2(-6.8f, -5.2f);
    }
    public void Card1Chosen() //character card 1
    {
        PM_C.position = new Vector2(-1f, 5.5f);
    }
    public void Card2Chosen() //character card 2
    {
        PM_C.position = new Vector2(3.4f, 5.5f);
    }
    public void Card3Chosen() //character card 3
    {
        PM_C.position = new Vector2(-1f, -1f);
    }
    public void Card4Chosen() //character card 4
    {
        PM_C.position = new Vector2(3.4f, -1f);
    }

    public float TimerCap
    {
        get
        {
            return TimerCapHit;
        }
    }

    public float TimerTracked
    {
        get
        {
            return TimerTracking;
        }
    }
}
