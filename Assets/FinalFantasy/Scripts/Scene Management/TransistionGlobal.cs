﻿/*-------------------------------------------------------------------------------------------------------------------------------
* Name: Kyle Harrington
 * Date: 1/17/2018
 * Credit: Project : FinalFantasy 1 Nes Remake
 * Purpose: This script handles the transitions between scenes, playing a black screen transition between them
 * 
 --------------------------------------------------------------------------------------------------------------------------*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransistionGlobal : MonoBehaviour {

    //Calls the "Scene" Objects
    [SerializeField]
    private PM_Cursor ResetTime;
    [SerializeField]
    private GameObject Overworld;
    [SerializeField]
    private GameObject PauseMenu;
    [SerializeField]
    private GameObject BattleScene;
    [SerializeField]
    private GameObject InventoryInPause;
    [SerializeField]
    private GameObject StatusInPause;
    private bool InvOff = false;
    private bool StatusOff = false;
    [SerializeField]
    private Image FadeObject;

	// Use this for initialization
    // Sets the FadeObject used for transitions to be transparent
	void Start () {
        FadeObject.canvasRenderer.SetAlpha(0.0f);
	}
	
	// Update is called once per frame
	void Update ()
        //detects what objects are currently active
    {
        if (InventoryInPause.activeInHierarchy == true)
        {
            InvOff = false;
        }
        else if (InventoryInPause.activeInHierarchy == false)
        {
            InvOff = true; 
        }
        if (StatusInPause.activeInHierarchy == true)
        {
            StatusOff = false;
        }
        else if (StatusInPause.activeInHierarchy == false)
        {
            StatusOff = true;
        }
        //detects input from the player and activates function to cause a transition between 2 scene objects
        PauseToOverworld();
        OverworldToPause();
	}

    void PauseToOverworld()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && PauseMenu.activeSelf && InvOff == false)
        {
            InventoryInPause.SetActive(false);
            ResetTime.ResetTimer();
        }
        if(Input.GetKeyDown(KeyCode.Escape) && PauseMenu.activeSelf && StatusOff == false)
        {
            StatusInPause.SetActive(false);
            ResetTime.ResetTimer();
        }
        if (Input.GetKeyDown(KeyCode.Escape) && PauseMenu.activeSelf && InvOff == true && StatusOff == true)
        {
            TransitionStart();
            PauseMenu.SetActive(false);
            Overworld.SetActive(true);
            ResetTime.ResetTimer();
            TransitionEnd();
        }
    }

    void OverworldToPause()
    {
        if (Input.GetKeyDown(KeyCode.Return) && Overworld.activeSelf)
        {
            TransitionStart();
            PauseMenu.SetActive(true);
            Overworld.SetActive(false);
            ResetTime.ResetTimer();
            TransitionEnd();
        }
    }

    void TownToOverworld()
    {

    }

    void OverworldToTown()
    {

    }
    
    public void OverworldToBattle()
    {
        TransitionStart();
        BattleScene.SetActive(true);
        Overworld.SetActive(false);
        TransitionEnd();
    }
    
    public void BattleToOverworld()
    {
        TransitionStart();
        BattleScene.SetActive(false);
        Overworld.SetActive(true);
        TransitionEnd();
    }

    void TransitionStart()
    {
        FadeObject.CrossFadeAlpha(1.0f, 1.0f, true);
    }

    void TransitionEnd()
    {
        FadeObject.CrossFadeAlpha(0.0f, 1.0f, true);
    }
}
