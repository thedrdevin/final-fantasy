﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Code by Nicholas Chaparro
//This script handles the transition for each sceen
//when the player moves into a trigger
//2-21-2018

public class MoveScene : MonoBehaviour {

    [SerializeField] private string loadLevel;
    public int switchSceneNumber;

	void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            SceneManager.LoadScene(loadLevel);
        }
    }
}
