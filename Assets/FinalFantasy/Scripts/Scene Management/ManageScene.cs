﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageScene : MonoBehaviour {

    public static ManageScene instance = null;
    public int currentSceneNumber;
    public GameObject player;
    public GameObject[] sceneSwitchArray;

	// Use this for initialization
	void Start ()
    {
	if(instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
    else if (instance != null)
        {
            Destroy(gameObject);
        }
        if (player == null)
            player = GameObject.FindGameObjectWithTag("Player");
        if (sceneSwitchArray.Length == 0)
            sceneSwitchArray = GameObject.FindGameObjectsWithTag("SceneSwitch");
	}
    void OnLevelWasLoaded() 
    {
        player = GameObject.FindGameObjectWithTag("Player");
        sceneSwitchArray = GameObject.FindGameObjectsWithTag("SceneSwitch");

        for(int i = 0; i < sceneSwitchArray.Length; i++)
        {
            if(sceneSwitchArray[i].GetComponent<MoveScene>().switchSceneNumber == currentSceneNumber)
            {
                player.transform.position = sceneSwitchArray[i].transform.position;
            }
        }
    }
    public void LoadScene (int passedSceneNumber)
    {
        currentSceneNumber = passedSceneNumber;
        if (Application.loadedLevel == 1)
        {
            Application.LoadLevel (12);
        }
        else if (Application.loadedLevel == 12)
        {
            Application.LoadLevel(1);
        }


        
    }
}
