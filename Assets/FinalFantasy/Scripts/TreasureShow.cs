﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TreasureShow : MonoBehaviour
{

    //Code By: Rahul Yerramneedi

    public bool isImgOn;
    public Image img;

    void Start()
    {

        img.enabled = false;
        isImgOn = false;
    }

    void Update()
    {

        //if (Input.GetKeyDown(KeyCode.Space))
        //{

        //    if (isImgOn == false)
        //    {

        //        img.enabled = true;
        //        isImgOn = true;
        //        Time.timeScale = 0;
        //    }

        //    else
        //    {

        //        img.enabled = false;
        //        isImgOn = false;
        //        Time.timeScale = 1;
        //    }
        //}


        if (Input.GetKeyDown(KeyCode.G))
        {
            if(isImgOn == true)
            {
                img.enabled = false;
                isImgOn = false;
                Time.timeScale = 1;
            }
        }
    }


    void OnTriggerStay2D(Collider2D collision)
    {
        if (Input.GetKeyDown(KeyCode.P))
        {

            if (isImgOn == false)
            {

                img.enabled = true;
                isImgOn = true;
                Time.timeScale = 0;
                
            }

            //else
            //{

            //    img.enabled = false;
            //    isImgOn = false;
            //}
        }

        //else if (Input.GetKeyDown(KeyCode.O))
        //{
        //    if (isImgOn == true)
        //    {
        //        img.enabled = false;
        //        isImgOn = false;
        //        Time.timeScale = 1;
        //    }
        //}
    }
}
