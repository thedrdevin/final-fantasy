﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*-------------------------------------------------------------------------------------------------------------------------------
* Name: Dongwon Yoo
 * Date: 1/1/16
 * Credit: Final Fantasy 1
 * Purpose: This Script is Changing position of the cursor.
 * Edited by Mike Looney to add in states for drinks and players to receive them
 --------------------------------------------------------------------------------------------------------------------------*/

public class DY_Cursor : MonoBehaviour
{
    private Transform tf;
    [SerializeField] private float timerReachPoint;
    private float timerTracker;
    SpriteRenderer spr;
    public GameObject cursor;

    // Use this for initialization
    void Start ()
    {
        tf = GetComponent<Transform>();
        spr = GetComponent<SpriteRenderer>();
        ResetTimer();
	}
	
	// Update is called once per frame
	void Update ()
    {
        timerTracker += Time.deltaTime;
	}

    public void ResetTimer()
    {
        timerTracker = 0;
    }

    //cursor will be dissapeared
    public void Dissapearing()
    {
        cursor.SetActive(false);
        //spr.sprite = Resources.Load("None", typeof(Sprite)) as Sprite;
    }

    //cusor will be appeared
    public void Appearing()
    {
        cursor.SetActive(true);
        //spr.sprite = Resources.Load("BattleSceneUI/Cursor", typeof(Sprite)) as Sprite;
    }

    //XXChosen are setting the Vector2D position of the cursor
    public void FightChosen()
    {
        tf.position = new Vector2(-1.3f, -2.1f);
    }

    public void MagicChosen()
    {
        tf.position = new Vector2(-1.3f, -2.8f);
    }

    public void DrinkChosen()
    {
        tf.position = new Vector2(-1.3f, -3.5f);
    }

    public void DrinkHealChosen()
    {
        tf.position = new Vector2(-4.6f, -2.4f);
    }

    public void Player1DrinkChosen()
    {
        tf.position = new Vector2(1, 2.2f);
    }

    public void Player2DrinkChosen()
    {
        tf.position = new Vector2(1, 1.2f);
    }

    public void Player3DrinkChosen()
    {
        tf.position = new Vector2(1, 0.2f);
    }

    public void Player4DrinkChosen()
    {
        tf.position = new Vector2(1, -1.0f);
    }

    public void ItemChosen()
    {
        tf.position = new Vector2(-1.3f, -4.2f);
    }

    public void RunChosen()
    {
        tf.position = new Vector2(1, -2.1f);
    }

    public void Mon1Chosen()
    {
        tf.position = new Vector2(-5.3f, 2.6f);
    }

    public void Mon2Chosen()
    {
        tf.position = new Vector2(-5.3f, 1.3f);
    }

    public void Mon3Chosen()
    {
        tf.position = new Vector2(-5.3f, 0);
    }

    public void Mon4Chosen()
    {
        tf.position = new Vector2(-4, 2.6f);
    }

    public void Mon5Chosen()
    {
        tf.position = new Vector2(-4, 1.3f);
    }

    public void Mon6Chosen()
    {
        tf.position = new Vector2(-4, 0);
    }

    public void Mon7Chosen()
    {
        tf.position = new Vector2(-2.8f, 2.6f);
    }

    public void Mon8Chosen()
    {
        tf.position = new Vector2(-2.8f, 1.3f);
    }

    public void Mon9Chosen()
    {
        tf.position = new Vector2(-2.8f, 0);
    }

    public float TimerReach
    {
        get
        {
            return timerReachPoint;
        }
    }

    public float TimeTracker
    {
        get
        {
            return timerTracker;
        }
    }
}
