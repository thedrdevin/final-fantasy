﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//created by Dominic Christiano if both need to appear or disappear at the same time
public class DC_DoorsIfBothTrue : MonoBehaviour {

    [SerializeField]
    public GameObject door;
    [SerializeField]
    public GameObject ceiling;
    [SerializeField]
    public GameObject player;

    

    //If needed switch the stay and leave based on what the situation calls for
   
    //when the player is in the collider both objects disappear
    void OnTriggerStay2D(Collider2D inside)
    {
        if (inside.gameObject.tag == "Player")
        {
            door.SetActive(false);
            ceiling.SetActive(false);
        }

    }

    //when the player is leaving the collider the objects both appear
    void OnTriggerExit2D(Collider2D leaving)
    {
        if (leaving.gameObject.tag == "Player")
        {
            door.SetActive(true);
            ceiling.SetActive(true);
        }
    }
}
