﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/*-------------------------------------------------------------------------------------------------------------------------------
* Name: Dongwon Yoo
 * Date: 1/1/16
 * Credit: Final Fantasy 1
 * Purpose: This Script is Changing the background image for the battle scene.
 * Edited and continued by Mike Looney
  Includes cursor states to select drinks and players to receive them*/
  /*---------------------------------------------------------------------------------------------------------------
  Edited by Mike Looney to include states for the cursor to select drinks and the characters to receive them
  -------------------------------------------------------------------------------------------------------------------------------*/
public class DY_ButtonNavigation : MonoBehaviour
{
    private DY_Cursor cursor;
    enum STATES { FightButton, MagicButton, DrinkButton, ItemButton, RunButton, Monster1, Monster2, Monster3, Monster4, Monster5, Monster6, Monster7, Monster8, Monster9
    , Running, DrinkSelect, Player1Drink, Player2Drink, Player3Drink, Player4Drink, NoItems}
    STATES curState = STATES.FightButton;
    Dictionary<STATES, Action> stateRegistry = new Dictionary<STATES, Action>();
    MonsterIdentifier mon1;                    // Stats for the monsters
    MonsterIdentifier mon2;                    // Stats for the monsters
    MonsterIdentifier mon3;                    // Stats for the monsters
    MonsterIdentifier mon4;                    // Stats for the monsters
    MonsterIdentifier mon5;                    // Stats for the monsters
    MonsterIdentifier mon6;                    // Stats for the monsters
    MonsterIdentifier mon7;                    // Stats for the monsters
    MonsterIdentifier mon8;                    // Stats for the monsters
    MonsterIdentifier mon9;                    // Stats for the monsters
    DY_TurnOrder turnOrder;
    DY_MonsterBehaviour monsterTarget;
    DY_MonsterGroup monsterGroup;
    

    private bool fight; // checks whether fight button is pressed
    private bool fightUP; // checks whether the player wants to choose upper one
    private bool fightDown; // checks whether the player wants to choose lower one
    private string previousButton; // get previous button before choosing running button, so that player can go back to the previous button by pressing left button

    public GameObject DrinkTextBox;
    public GameObject NoItems;
    private L_ShopCursorNav otherScript;

    private bool running; // check whether the player wants to flee

    private void OnDisable()
    {
        fight = false;
        fightUP = false;
        fightDown = false;
        running = false;
        previousButton = "";
        SetState(STATES.FightButton);
    }

    private void OnEnable()
    {
        SetState(STATES.FightButton);
    }

    // Use this for initialization
    void Start()
    {
        monsterGroup = GameObject.Find("MonsterGroup").GetComponent<DY_MonsterGroup>();
        monsterTarget = GameObject.Find("MonsterBehave").GetComponent<DY_MonsterBehaviour>();
        cursor = GameObject.Find("CursorForBattle").GetComponent<DY_Cursor>();
        stateRegistry.Add(STATES.FightButton, new Action(FightButtonState));
        stateRegistry.Add(STATES.MagicButton, new Action(MagicButtonState));
        stateRegistry.Add(STATES.DrinkButton, new Action(DrinkButtonState));
        stateRegistry.Add(STATES.ItemButton, new Action(ItemButtonState));
        stateRegistry.Add(STATES.RunButton, new Action(RunButtonState));
        stateRegistry.Add(STATES.DrinkSelect, new Action(DrinkSelectState));
        stateRegistry.Add(STATES.Monster1, new Action(Mon1));
        stateRegistry.Add(STATES.Monster2, new Action(Mon2));
        stateRegistry.Add(STATES.Monster3, new Action(Mon3));
        stateRegistry.Add(STATES.Monster4, new Action(Mon4));
        stateRegistry.Add(STATES.Monster5, new Action(Mon5));
        stateRegistry.Add(STATES.Monster6, new Action(Mon6));
        stateRegistry.Add(STATES.Monster7, new Action(Mon7));
        stateRegistry.Add(STATES.Monster8, new Action(Mon8));
        stateRegistry.Add(STATES.Monster9, new Action(Mon9));
        stateRegistry.Add(STATES.Running, new Action(Running));
        stateRegistry.Add(STATES.Player1Drink, new Action(Player1DrinkState));
        stateRegistry.Add(STATES.Player2Drink, new Action(Player2DrinkState));
        stateRegistry.Add(STATES.Player3Drink, new Action(Player3DrinkState));
        stateRegistry.Add(STATES.Player4Drink, new Action(Player4DrinkState));
        stateRegistry.Add(STATES.NoItems, new Action(NoItemsState));
        mon1 = GameObject.Find("Monster1").GetComponent<MonsterIdentifier>();
        mon2 = GameObject.Find("Monster2").GetComponent<MonsterIdentifier>();
        mon3 = GameObject.Find("Monster3").GetComponent<MonsterIdentifier>();
        mon4 = GameObject.Find("Monster4").GetComponent<MonsterIdentifier>();
        mon5 = GameObject.Find("Monster5").GetComponent<MonsterIdentifier>();
        mon6 = GameObject.Find("Monster6").GetComponent<MonsterIdentifier>();
        mon7 = GameObject.Find("Monster7").GetComponent<MonsterIdentifier>();
        mon8 = GameObject.Find("Monster8").GetComponent<MonsterIdentifier>();
        mon9 = GameObject.Find("Monster9").GetComponent<MonsterIdentifier>();
        turnOrder = GameObject.Find("TurnOrder").GetComponent<DY_TurnOrder>();
        SetState(STATES.FightButton);
        
    }

    // Update is called once per frame
    void Update()
    {
        stateRegistry[curState].Invoke();

        if (curState == STATES.FightButton)
        {
            FightButtonState();
        }
        else if (curState == STATES.MagicButton)
        {
            MagicButtonState();
        }
        else if (curState == STATES.DrinkButton)
        {
            DrinkButtonState();
        }
        else if (curState == STATES.ItemButton)
        {
            ItemButtonState();
        }
        else if (curState == STATES.RunButton)
        {
            RunButtonState();
        }
        else if (curState == STATES.Monster1)
        {
            Mon1();
        }
        else if (curState == STATES.Monster2)
        {
            Mon2();
        }
        else if (curState == STATES.Monster3)
        {
            Mon3();
        }
        else if (curState == STATES.Monster4)
        {
            Mon4();
        }
        else if (curState == STATES.Monster5)
        {
            Mon5();
        }
        else if (curState == STATES.Monster6)
        {
            Mon6();
        }
        else if (curState == STATES.Monster7)
        {
            Mon7();
        }
        else if (curState == STATES.Monster8)
        {
            Mon8();
        }
        else if (curState == STATES.Monster9)
        {
            Mon9();
        }
        else if (curState == STATES.Running)
        {
            Running();
        }

        if (fight)
        {
            // if player presses X, the cursor will go back to previous selection menu
            if (Input.GetKeyDown(KeyCode.X))
            {
                fight = false;
                SetState(STATES.FightButton);
                cursor.ResetTimer();
                cursor.FightChosen();
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (cursor.TimerReach <= cursor.TimeTracker)
                {
                    fightUP = true;
                    cursor.Dissapearing();
                    cursor.ResetTimer();
                    if (curState == STATES.Monster1)
                    {
                        SetState(STATES.Monster9);
                    }
                    else if(curState == STATES.Monster2)
                    {
                        SetState(STATES.Monster1);
                    }
                    else if (curState == STATES.Monster3)
                    {
                        SetState(STATES.Monster2);
                    }
                    else if (curState == STATES.Monster4)
                    {
                        SetState(STATES.Monster3);
                    }
                    else if (curState == STATES.Monster5)
                    {
                        SetState(STATES.Monster4);
                    }
                    else if (curState == STATES.Monster6)
                    {
                        SetState(STATES.Monster5);
                    }
                    else if (curState == STATES.Monster7)
                    {
                        SetState(STATES.Monster6);
                    }
                    else if (curState == STATES.Monster8)
                    {
                        SetState(STATES.Monster7);
                    }
                    else if (curState == STATES.Monster9)
                    {
                        SetState(STATES.Monster8);
                    }
                }
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (cursor.TimerReach <= cursor.TimeTracker)
                {
                    fightDown = true;
                    cursor.Dissapearing();
                    cursor.ResetTimer();
                    if (curState == STATES.Monster1)
                    {
                        SetState(STATES.Monster2);
                    }
                    else if (curState == STATES.Monster2)
                    {
                        SetState(STATES.Monster3);
                    }
                    else if (curState == STATES.Monster3)
                    {
                        SetState(STATES.Monster4);
                    }
                    else if (curState == STATES.Monster4)
                    {
                        SetState(STATES.Monster5);
                    }
                    else if (curState == STATES.Monster5)
                    {
                        SetState(STATES.Monster6);
                    }
                    else if (curState == STATES.Monster6)
                    {
                        SetState(STATES.Monster7);
                    }
                    else if (curState == STATES.Monster7)
                    {
                        SetState(STATES.Monster8);
                    }
                    else if (curState == STATES.Monster8)
                    {
                        SetState(STATES.Monster9);
                    }
                    else if (curState == STATES.Monster9)
                    {
                        SetState(STATES.Monster1);
                    }
                }
            }
        }
    }

    void ResetStateToFightButton()
    {
        SetState(STATES.FightButton);
        cursor.FightChosen();
        cursor.ResetTimer();
    }

    void SetState(STATES newState)
    {
        curState = newState;
    }

    void FightButtonState()
    {
        previousButton = "FightButton";
        if (cursor.TimerReach <= cursor.TimeTracker)
        {
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                cursor.RunChosen();
                SetState(STATES.RunButton);
                cursor.ResetTimer();
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                cursor.MagicChosen();
                SetState(STATES.MagicButton);
                cursor.ResetTimer();
            }
            else if (Input.GetKeyDown(KeyCode.Z))
            {
                cursor.Dissapearing();
                SetState(STATES.Monster1);
                cursor.ResetTimer();
                fight = true;
                fightDown = true;
            }
        }
    }

    void MagicButtonState()
    {
        previousButton = "MagicButton";
        if (cursor.TimerReach <= cursor.TimeTracker)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                cursor.FightChosen();
                SetState(STATES.FightButton);
                cursor.ResetTimer();
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                cursor.DrinkChosen();
                SetState(STATES.DrinkButton);
                cursor.ResetTimer();
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                cursor.RunChosen();
                SetState(STATES.RunButton);
                cursor.ResetTimer();
            }
        }
    }

    void DrinkButtonState() 
    {
        previousButton = "DrinkButton";
        if (cursor.TimerReach <= cursor.TimeTracker)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                cursor.MagicChosen();
                SetState(STATES.MagicButton);
                cursor.ResetTimer();
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                cursor.ItemChosen();
                SetState(STATES.ItemButton);
                cursor.ResetTimer();
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                cursor.RunChosen();
                SetState(STATES.RunButton);
                cursor.ResetTimer();
            }
            else if (Input.GetKeyDown(KeyCode.Z))
            {
                    cursor.DrinkHealChosen();
                    DrinkTextBox.SetActive(true);
                    SetState(STATES.DrinkSelect);

            }
        }
    }

    void NoItemsState()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            cursor.FightChosen();
            SetState(STATES.FightButton);
            cursor.ResetTimer();
            NoItems.SetActive(false);
        }
        else if (Input.GetKeyDown(KeyCode.X))
        {
            cursor.FightChosen();
            SetState(STATES.FightButton);
            cursor.ResetTimer();
            NoItems.SetActive(false);

        }
    }

    void DrinkSelectState() //Sets the cursor to choose which item to select
    {
        if (cursor.TimerReach <= cursor.TimeTracker)
        {
            if (Input.GetKeyDown(KeyCode.Z))
            {
                cursor.Player1DrinkChosen();
                SetState(STATES.Player1Drink);
                DrinkTextBox.SetActive(false);
            }
            else if (Input.GetKeyDown(KeyCode.X))
            {
                cursor.DrinkChosen();
                DrinkTextBox.SetActive(false);
                SetState(STATES.DrinkButton);
                cursor.ResetTimer();
            }
        }
    }
    void Player1DrinkState() //Sets the cursor to choose which player receives an item
    {
        if (cursor.TimerReach <= cursor.TimeTracker)
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                cursor.Player2DrinkChosen();
                SetState(STATES.Player2Drink);
                cursor.ResetTimer();
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                cursor.Player4DrinkChosen();
                SetState(STATES.Player4Drink);
                cursor.ResetTimer();
            }
            else if (Input.GetKeyDown(KeyCode.Z))
            {

            }
            else if (Input.GetKeyDown(KeyCode.X))
            {
                cursor.DrinkHealChosen();
                DrinkTextBox.SetActive(true);
                SetState(STATES.DrinkSelect);
                cursor.ResetTimer();
            }
        }
    }

    void Player2DrinkState()
    {
        if (cursor.TimerReach <= cursor.TimeTracker)
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                cursor.Player3DrinkChosen();
                SetState(STATES.Player3Drink);
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                cursor.Player1DrinkChosen();
                SetState(STATES.Player1Drink);
                cursor.ResetTimer();
            }
            else if (Input.GetKeyDown(KeyCode.X))
            {
                cursor.DrinkHealChosen();
                DrinkTextBox.SetActive(true);
                SetState(STATES.DrinkSelect);
                cursor.ResetTimer();
            }
        }
    }

    void Player3DrinkState()
    {
        if (cursor.TimerReach <= cursor.TimeTracker)
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                cursor.Player4DrinkChosen();
                SetState(STATES.Player4Drink);
                cursor.ResetTimer();
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                cursor.Player2DrinkChosen();
                SetState(STATES.Player2Drink);
                cursor.ResetTimer();
            }
            else if (Input.GetKeyDown(KeyCode.X))
            {
                cursor.DrinkHealChosen();
                DrinkTextBox.SetActive(true);
                SetState(STATES.DrinkSelect);
                cursor.ResetTimer();
            }
        }
    }

    void Player4DrinkState()
    {
        if (cursor.TimerReach <= cursor.TimeTracker)
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                cursor.Player1DrinkChosen();
                SetState(STATES.Player1Drink);
                cursor.ResetTimer();
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                cursor.Player3DrinkChosen();
                SetState(STATES.Player3Drink);
                cursor.ResetTimer();
            }
            else if (Input.GetKeyDown(KeyCode.X))
            {
                cursor.DrinkHealChosen();
                DrinkTextBox.SetActive(true);
                SetState(STATES.DrinkSelect);
                cursor.ResetTimer();
            }
        }
    }

    void ItemButtonState()
    {
        previousButton = "ItemButton";
        if (cursor.TimerReach <= cursor.TimeTracker)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                cursor.DrinkChosen();
                SetState(STATES.DrinkButton);
                cursor.ResetTimer();
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                cursor.RunChosen();
                SetState(STATES.RunButton);
                cursor.ResetTimer();
            }
        }
    }

    void RunButtonState()
    {
        if (cursor.TimerReach <= cursor.TimeTracker)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if (previousButton == "FightButton")
                {
                    cursor.FightChosen();
                    SetState(STATES.FightButton);
                    cursor.ResetTimer();
                }
                else if (previousButton == "MagicButton")
                {
                    cursor.MagicChosen();
                    SetState(STATES.MagicButton);
                    cursor.ResetTimer();
                }
                else if (previousButton == "DrinkButton")
                {
                    cursor.DrinkChosen();
                    SetState(STATES.DrinkButton);
                    cursor.ResetTimer();
                }
                else if(previousButton == "ItemButton")
                {
                    cursor.ItemChosen();
                    SetState(STATES.ItemButton);
                    cursor.ResetTimer();
                }
            }
            else if (Input.GetKeyDown(KeyCode.Z))
            {
                if (!monsterGroup.Unrunnable)
                {
                    cursor.Dissapearing();
                    SetState(STATES.Running);
                    cursor.ResetTimer();
                }
                else
                {
                    cursor.FightChosen();
                    SetState(STATES.FightButton);
                    Debug.Log("You can't Run away");
                    cursor.ResetTimer();
                }
            }
        }
    }

    // when cursor is on the monster#
    void Mon1()
    {
        if (mon1.Dead) // if that monster is dead
        {
            if (fightUP)
            {
                SetState(STATES.Monster9); // if the player pressed up button, go to monster9 state
            }
            else if (fightDown)
            {
                SetState(STATES.Monster2); // if the player pressed down button, go to monster2 state
            }
        }
        else
        {
            cursor.Appearing();
            cursor.Mon1Chosen();
            fightUP = false;
            fightDown = false;
            if (Input.GetKeyDown(KeyCode.Z) && cursor.TimerReach <= cursor.TimeTracker)
            {
                ResetStateToFightButton(); // set state to fight button
                turnOrder.TurnOver(); // turn sequences will happen(Check DY_MonsterBehaviour Script)
                monsterTarget.TargettedMonster = 0; // this will tell DY_MonsterBehaviour which monster has chosen
                monsterTarget.MonChosen++; // this is the amount of chosen. if this hits 4 or amount of player, enemy or player will start to attack.
            }
        }
    }

    void Mon2()
    {
        if (mon2.Dead)
        {
            if (fightUP)
            {
                SetState(STATES.Monster1);
            }
            else if (fightDown)
            {
                SetState(STATES.Monster3);
            }
        }
        else
        {
            cursor.Appearing();
            cursor.Mon2Chosen();
            fightUP = false;
            fightDown = false;
            if (Input.GetKeyDown(KeyCode.Z) && cursor.TimerReach <= cursor.TimeTracker)
            {
                ResetStateToFightButton();
                turnOrder.TurnOver();
                monsterTarget.TargettedMonster = 1;
                monsterTarget.MonChosen++;
            }
        }
    }

    void Mon3()
    {
        if (mon3.Dead)
        {
            if (fightUP)
            {
                SetState(STATES.Monster2);
            }
            else if (fightDown)
            {
                SetState(STATES.Monster4);
            }
        }
        else
        {
            cursor.Appearing();
            cursor.Mon3Chosen();
            fightUP = false;
            fightDown = false;
            if (Input.GetKeyDown(KeyCode.Z) && cursor.TimerReach <= cursor.TimeTracker)
            {
                ResetStateToFightButton();
                turnOrder.TurnOver();
                monsterTarget.TargettedMonster = 2;
                monsterTarget.MonChosen++;
            }
        }
    }

    void Mon4()
    {
        if (mon4.Dead)
        {
            if (fightUP)
            {
                SetState(STATES.Monster3);
            }
            else if (fightDown)
            {
                SetState(STATES.Monster5);
            }
        }
        else
        {
            cursor.Appearing();
            cursor.Mon4Chosen();
            fightUP = false;
            fightDown = false;
            if (Input.GetKeyDown(KeyCode.Z) && cursor.TimerReach <= cursor.TimeTracker)
            {
                ResetStateToFightButton();
                turnOrder.TurnOver();
                monsterTarget.TargettedMonster = 3;
                monsterTarget.MonChosen++;
            }
        }
    }

    void Mon5()
    {
        if (mon5.Dead)
        {
            if (fightUP)
            {
                SetState(STATES.Monster4);
            }
            else if (fightDown)
            {
                SetState(STATES.Monster6);
            }
        }
        else
        {
            cursor.Appearing();
            cursor.Mon5Chosen();
            fightUP = false;
            fightDown = false;
            if (Input.GetKeyDown(KeyCode.Z) && cursor.TimerReach <= cursor.TimeTracker)
            {
                ResetStateToFightButton();
                turnOrder.TurnOver();
                monsterTarget.TargettedMonster = 4;
                monsterTarget.MonChosen++;
            }
        }
    }

    void Mon6()
    {
        if (mon6.Dead)
        {
            if (fightUP)
            {
                SetState(STATES.Monster5);
            }
            else if (fightDown)
            {
                SetState(STATES.Monster7);
            }
        }
        else
        {
            cursor.Appearing();
            cursor.Mon6Chosen();
            fightUP = false;
            fightDown = false;
            if (Input.GetKeyDown(KeyCode.Z) && cursor.TimerReach <= cursor.TimeTracker)
            {
                ResetStateToFightButton();
                turnOrder.TurnOver();
                monsterTarget.TargettedMonster = 5;
                monsterTarget.MonChosen++;
            }
        }
    }

    void Mon7()
    {
        if (mon7.Dead)
        {
            if (fightUP)
            {
                SetState(STATES.Monster6);
            }
            else if (fightDown)
            {
                SetState(STATES.Monster8);
            }
        }
        else
        {
            cursor.Appearing();
            cursor.Mon7Chosen();
            fightUP = false;
            fightDown = false;
            if (Input.GetKeyDown(KeyCode.Z) && cursor.TimerReach <= cursor.TimeTracker)
            {
                ResetStateToFightButton();
                turnOrder.TurnOver();
                monsterTarget.TargettedMonster = 6;
                monsterTarget.MonChosen++;
            }
        }
    }

    void Mon8()
    {
        if (mon8.Dead)
        {
            if (fightUP)
            {
                SetState(STATES.Monster7);
            }
            else if (fightDown)
            {
                SetState(STATES.Monster9);
            }
        }
        else
        {
            cursor.Appearing();
            cursor.Mon8Chosen();
            fightUP = false;
            fightDown = false;
            if (Input.GetKeyDown(KeyCode.Z) && cursor.TimerReach <= cursor.TimeTracker)
            {
                ResetStateToFightButton();
                turnOrder.TurnOver();
                monsterTarget.TargettedMonster = 7;
                monsterTarget.MonChosen++;
            }
        }
    }

    void Mon9()
    {
        if (mon8.Dead)
        {
            if (fightUP)
            {
                SetState(STATES.Monster8);
            }
            else if (fightDown)
            {
                SetState(STATES.Monster1);
            }
        }
        else
        {
            cursor.Appearing();
            cursor.Mon9Chosen();
            fightUP = false;
            fightDown = false;
            if (Input.GetKeyDown(KeyCode.Z) && cursor.TimerReach <= cursor.TimeTracker)
            {
                ResetStateToFightButton();
                turnOrder.TurnOver();
                monsterTarget.TargettedMonster = 8;
                monsterTarget.MonChosen++;
            }
        }
    }

    void Running()
    {
        turnOrder.WannaRun = true;
        cursor.Appearing();
        cursor.FightChosen();
        SetState(STATES.FightButton);
        cursor.ResetTimer();
        turnOrder.TurnOver();
    }
}
