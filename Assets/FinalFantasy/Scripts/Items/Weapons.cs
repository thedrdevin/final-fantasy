﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapons : MonoBehaviour {

    //Commented Out Enums for weapons
    /* Enums for Weapons/Types
    public sWeapons weapon;
    public enum sWeapons
    {
        Sword,
        Knife,
        Axe,
        Hammer,
        Staff,
        Nunchucks,

    }

    public enum SwordTypes
    {
        Short,
        Iron,
        Long,
        Silver,
        Flame,
        Ice,
        Dragon,
        Sun,
        Coral,
        Were,
        Rune,
        Bane,
    }
    public enum KnifeTypes
    {
        Small,
        Large,
        Silver,
    }
    public enum AxeType
    {
        Hand,
        Great,
        Silver,
        Light,
    }
    public enum HammerType
    {
        Iron,
        Silver,
        Thor,
    }
    public enum StaffType
    {
        Wooden,
        Power,
        Wizard,
    }
    public enum NunchucksType
    {
        Wooden,
        Iron,
    }
    */


    public GameObject[] tool = new GameObject[40];

    public GameObject[] Sword = new GameObject[13];
    public GameObject[] Knife = new GameObject[3];
    public GameObject[] Axe = new GameObject[4];
    public GameObject[] Hammer = new GameObject[3];
    public GameObject[] Staff = new GameObject[4];
    public GameObject[] Nunchuck = new GameObject[2];
    public GameObject[] Specials = new GameObject[10];
    


    // Use this for initialization
    void Start ()
    {
       

    }
	
	// Update is called once per frame
	void Update () {
		
	}


    // Organize all weapons into Array to be indexed
    void WeaponOrganizer()
    {
        // S = Sword 13
        // K = Knife 3
        // A = Axe 4
        // H = Hammer 3
        // R = Staff 4
        // N = Nunchuck 2
        // _ = Specials 10

                                // Index #  Type []

        tool[0] = Nunchuck[0];  // 01 Wooden [N]
        tool[1] = Knife[0];     // 02 Small [K] 
        tool[2] = Staff[0];     // 03 Wooden [R] 
        tool[3] = Specials[0];  // 04 Rapier 1
        tool[4] = Hammer[0];    // 05 Iron [H] 
        tool[5] = Sword[0];     // 06 Short [S] 
        tool[6] = Axe[0];       // 07 Hand [A] 

 /* Not Using the rest of the weapons
        tool[7] = Specials[1];     // 08 Schimtar 
        tool[8] = Nunchuck[1];     // 09 Iron [N] 
        tool[9] = Knife[1];     // 10 Large [K] 
        tool[10] = Sword[1];    // 11 Iron [S] 
        tool[11] = Specials[2]; // 12 Sabre 
        tool[12] = Sword[2];    // 13 Long [S] 
        tool[13] = Axe[1];      // 14 Great [A] 
        tool[14] = Specials[3]; // 15 Falchon 
        tool[15] = Knife[2];    // 16 Silver [K] 
        tool[16] = Sword[3];    // 17 Silver [S] 
        tool[17] = Hammer[1];   // 18 Silver [H] 
        tool[18] = Axe[2];      // 19 Silver [A] 
        tool[19] = Sword[4];    // 20 Flame [S]       
        tool[20] = Sword[5];    // 21 Ice [S] 
        tool[21] = Sword[6];    // 22 Dragon [S] 
        tool[22] = Sword[7];    // 23 Giant [S] 
        tool[23] = Sword[8];    // 24 Sun [S] 
        tool[24] = Sword[9];    // 25 Coral [S] 
        tool[25] = Sword[10];    // 26 Were [S] 
        tool[26] = Sword[11];    // 27 Rune [S] 
        tool[27] = Staff[1];    // 28 Power [R] 
        tool[28] = Axe[3];      // 29 Light [A] 
        tool[29] = Staff[1];    // 30 Heal [R] 
        tool[30] = Staff[2];    // 31 Mage [R] 
        tool[31] = Specials[4]; // 32 Defense 
        tool[32] = Staff[3];    // 33 Wizard [R] 
        tool[33] = Specials[5]; // 34 Vorpal 
        tool[34] = Specials[6]; // 35 CatClaw
        tool[35] = Hammer[2];   // 36 Thor [H]
        tool[36] = Sword[12];    // 37 Bane [S]
        tool[37] = Specials[7]; // 38 Katana
        tool[38] = Specials[8]; // 39 Xcaliber
        tool[39] = Specials[9]; // 40 Masume
*/
    } 
}
