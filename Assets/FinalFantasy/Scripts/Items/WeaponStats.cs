﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponStats : MonoBehaviour
{

    
    int ATK;
    float HitPercent;
    int CritChance;
    string Spell; // could be a float depending on how each spell affects the weapon/character
    int Price;

    public GameObject[] Weapon;

    // Use this for initialization
    void Start ()
    {
        Weapon[0] = GameObject.Find("Nunchuck_Wooden");
        Weapon[1] = GameObject.Find("Knife_Small");
        Weapon[2] = GameObject.Find("Staff_Wooden");
        Weapon[3] = GameObject.Find("Rapier");
        Weapon[4] = GameObject.Find("Hammer_Iron");
        Weapon[5] = GameObject.Find("Sword_Short");
        Weapon[6] = GameObject.Find("Axe_Hand");

        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void SetSword_Shot()
    {
        ATK = 15;
        HitPercent = 0.1f;
        CritChance = 5;
        Spell = "";
        Price = 550;
    }
    void SetNunchuck_Wooden()
    {
        ATK = 12;
        HitPercent = 0.0f;
        CritChance = 10;
        Spell = "";
        Price = 10;
    }
    void SetKnife_Small()
    {
        ATK = 5;
        HitPercent = 0.1f;
        CritChance = 5;
        Spell = "";
        Price = 5;
    }
    void SetRapier()
    {
        ATK = 9;
        HitPercent = 0.05f;
        CritChance = 10;
        Spell = "";
        Price = 10;
    }
    void SetHammer_Iron()
    {
        ATK = 9;
        HitPercent = 0.0f;
        CritChance = 1;
        Spell = "";
        Price = 10;
    }
    void SetAxe_Hand()
    {
        ATK = 16;
        HitPercent = 0.05f;
        CritChance = 3;
        Spell = "";
        Price = 550;

    }



















































}
