﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Hammer : MonoBehaviour
{

    public bool isImgOn;
    public Image img;

    void Start()
    {

        img.enabled = false;
        isImgOn = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {

            if (isImgOn == false)
            {

                img.enabled = true;
                isImgOn = true;
                Time.timeScale = 0;
            }

            else
            {

                img.enabled = false;
                isImgOn = false;
                Time.timeScale = 1;
            }
        }
    }
}
