﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*-------------------------------------------------------------------------------------------------------------------------------
* Name: Dongwon Yoo
 * Date: 1/1/16
 * Credit: Final Fantasy 1
 * Purpose: This Script is Changing the Background Image of the Battle Scene
 --------------------------------------------------------------------------------------------------------------------------*/

//This Script is Changing the Background Image of the Battle Scene
public class DY_BattleSceneUI : MonoBehaviour
{

    [SerializeField] private int battleSequence; // ex) if player is choosing what to do in the button scene, battle sequence would be 1

    SpriteRenderer spr;

	// Use this for initialization
	void Start ()
    {
        spr = GetComponent<SpriteRenderer>();
        spr.sprite = Resources.Load("BattleSceneUI/UI_1", typeof(Sprite)) as Sprite;
    }

    void OnEnable()
    {
        battleSequence = 0;
    }
	
	// Update is called once per frame
	void Update ()
    {
        //When Turn is changing
		if(battleSequence == 0)
        {
            spr.sprite = Resources.Load("BattleSceneUI/UI_1", typeof(Sprite)) as Sprite;
        }
        //Button Appears
        if (battleSequence == 1)
        {
            spr.sprite = Resources.Load("BattleSceneUI/UI_2", typeof(Sprite)) as Sprite;
        }
        //DamageExchange
        if (battleSequence == 2)
        {
            spr.sprite = Resources.Load("BattleSceneUI/UI_3", typeof(Sprite)) as Sprite;
        }
        //Victory
        if (battleSequence == 3)
        {
            spr.sprite = Resources.Load("BattleSceneUI/UI_4", typeof(Sprite)) as Sprite;
        }
        //Lose and Run
        if (battleSequence == 4)
        {
            spr.sprite = Resources.Load("BattleSceneUI/UI_5", typeof(Sprite)) as Sprite;
        }
        //Magic 
        if (battleSequence == 5)
        {
            spr.sprite = Resources.Load("BattleSceneUI/UI_6", typeof(Sprite)) as Sprite;
        }
        //LevelUp
        if (battleSequence == 6)
        {
            spr.sprite = Resources.Load("BattleSceneUI/UI_7", typeof(Sprite)) as Sprite;
        }
        //Name this whatever you want
        /*if (battleSequence == 7)
        {
            spr.sprite = Resources.Load("BattleSceneUI/UI_8", typeof(Sprite)) as Sprite;
        }
         */
    }

    public int BattleUI
    {
        get
        {
            return battleSequence;
        }
        set
        {
            battleSequence = value;
        }
    }
}
