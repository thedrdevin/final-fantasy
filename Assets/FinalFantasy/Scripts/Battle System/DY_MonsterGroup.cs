﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*-------------------------------------------------------------------------------------------------------------------------------
* Name: Dongwon Yoo
 * Date: 1/1/16
 * Credit: Final Fantasy 1
 * Purpose: This Script identifies which monster group to meet
 * 
 * Edited by Dominic Christiano to add Wolves/Wargs
 * Edited by Santiago Charry to add Gigas worm and Black Widow
 --------------------------------------------------------------------------------------------------------------------------*/

public class DY_MonsterGroup : MonoBehaviour
{
    [SerializeField] private int monsterGroupNum; // this identifies which monster group to meet
    [SerializeField] private int amountMons; // amount of monsters

    MonsterIdentifier monster1;
    MonsterIdentifier monster2;
    MonsterIdentifier monster3;
    MonsterIdentifier monster4;
    MonsterIdentifier monster5;
    MonsterIdentifier monster6;
    MonsterIdentifier monster7;
    MonsterIdentifier monster8;
    MonsterIdentifier monster9;

    [SerializeField] private bool fight;

    [SerializeField] private bool unrunnable; // player can't run if this variable is true


    // Use this for initialization
    void Start ()
    {
        monster1 = GameObject.Find("Monster1").GetComponent<MonsterIdentifier>();
        monster2 = GameObject.Find("Monster2").GetComponent<MonsterIdentifier>();
        monster3 = GameObject.Find("Monster3").GetComponent<MonsterIdentifier>();
        monster4 = GameObject.Find("Monster4").GetComponent<MonsterIdentifier>();
        monster5 = GameObject.Find("Monster5").GetComponent<MonsterIdentifier>();
        monster6 = GameObject.Find("Monster6").GetComponent<MonsterIdentifier>();
        monster7 = GameObject.Find("Monster7").GetComponent<MonsterIdentifier>();
        monster8 = GameObject.Find("Monster8").GetComponent<MonsterIdentifier>();
        monster9 = GameObject.Find("Monster9").GetComponent<MonsterIdentifier>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        //monsteridentity sets the monster object to which monster to be.
        //dead bools are the switchs for monster to be dissapeard
        if (!fight)
        {
            if (monsterGroupNum == 0)
            {
                monster1.Dead = false;
                monster2.Dead = false;
                monster3.Dead = false;
                monster4.Dead = false;
                monster5.Dead = false;
                monster6.Dead = false;
                monster7.Dead = false;
                monster8.Dead = false;
                monster9.Dead = false;
                monster1.MonsterIdentity = 0;
                monster2.MonsterIdentity = 0;
                monster3.MonsterIdentity = 0;
                monster4.MonsterIdentity = 0;
                monster5.MonsterIdentity = 0;
                monster6.MonsterIdentity = 0;
                monster7.Dead = true;
                monster8.Dead = true;
                monster9.Dead = true;
                amountMons = 6;
            }
            else if (monsterGroupNum == 1)
            {
                monster1.Dead = false;
                monster2.Dead = false;
                monster3.Dead = false;
                monster4.Dead = false;
                monster5.Dead = false;
                monster6.Dead = false;
                monster7.Dead = false;
                monster8.Dead = false;
                monster9.Dead = false;
                monster1.MonsterIdentity = 1;
                monster2.MonsterIdentity = 1;
                monster3.MonsterIdentity = 1;
                monster4.Dead = true;
                monster5.Dead = true;
                monster6.Dead = true;
                monster7.Dead = true;
                monster8.Dead = true;
                monster9.Dead = true;
                amountMons = 3;
            }
            else if (monsterGroupNum == 2)
            {
                monster1.Dead = false;
                monster2.Dead = false;
                monster3.Dead = false;
                monster4.Dead = false;
                monster5.Dead = false;
                monster6.Dead = false;
                monster7.Dead = false;
                monster8.Dead = false;
                monster9.Dead = false;
                monster1.Dead = true;
                monster2.Dead = true;
                monster3.Dead = true;
                monster4.Dead = true;
                monster5.MonsterIdentity = 3;
                monster6.Dead = true;
                monster7.Dead = true;
                monster8.Dead = true;
                monster9.Dead = true;
                amountMons = 1;
            }
            else if (monsterGroupNum == 3)
            {
                monster1.Dead = false;
                monster2.Dead = false;
                monster3.Dead = false;
                monster4.Dead = false;
                monster5.Dead = false;
                monster6.Dead = false;
                monster7.Dead = false;
                monster8.Dead = false;
                monster9.Dead = false;
                monster1.MonsterIdentity = 3;
                monster2.MonsterIdentity = 3;
                monster3.MonsterIdentity = 3;
                monster4.MonsterIdentity = 3;
                monster5.Dead = true;
                monster6.Dead = true;
                monster7.Dead = true;
                monster8.Dead = true;
                monster9.Dead = true;
                amountMons = 4;
            }
            else if (monsterGroupNum == 4)
            {
                monster1.Dead = false;
                monster2.Dead = false;
                monster3.Dead = false;
                monster4.Dead = false;
                monster5.Dead = false;
                monster6.Dead = false;
                monster7.Dead = false;
                monster8.Dead = false;
                monster9.Dead = false;
                monster1.MonsterIdentity = 4;
                monster2.MonsterIdentity = 4;
                monster3.Dead = true;
                monster4.Dead = true;
                monster5.Dead = true;
                monster6.Dead = true;
                monster7.Dead = true;
                monster8.Dead = true;
                monster9.Dead = true;
                amountMons = 2;
            }


            fight = true;
        }
    }

    public int MonsterGroup
    {
        get
        {
            return monsterGroupNum;
        }
        set
        {
            monsterGroupNum = value;
        }
    }

    public int AmountMons { get { return amountMons; } set { amountMons = value; } }
    public bool Unrunnable { get { return unrunnable; } set { unrunnable = value; } }
    public bool Fight { get { return fight; } set { fight = value; } }
}
