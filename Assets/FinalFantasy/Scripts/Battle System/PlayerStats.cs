﻿/*-------------------------------------------------------------------------------------------------------------------------------
* Name: Kyle Harrington
 * Date: 1/17/2018
 * Credit: Project : FinalFantasy 1 Nes Remake
 * Purpose: This script stores the stats for each object that it is attached too, namely the Character's on a players team.
 * 
 --------------------------------------------------------------------------------------------------------------------------*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class PlayerStats : MonoBehaviour {

    public PlayerStats myPlayerStats; //reference to self

    [SerializeField]
    private string CharacterName;   //character's in game name
    [SerializeField]                //player stats based off 1st 5 levels of the fighter/knight class
    private int PlayerLv = 1;       //character's current level stat
    [SerializeField]
    private int PlayerCurrentExp = 0; //character's current exp
    [SerializeField]
    private int PlayerNeededExp = 40; // How much more EXP is needed for a character to level up, triggerling the level up function
    private int PlayerMaxHP = 35;   //character's max hp stat, must not exceed this amount 
    private int PlayerCurrentHP;    //character's current hp stat
    private int PlayerMP = 0;       //characters magic power stat
    private int PlayerStr = 20;     //character's strength stat
    private int PlayerAgl = 5;      //character's agility stat
    private int PlayerInt = 1;      //character's intelligence stat
    private int PlayerVit = 10;     //character's vitality stat
    private int PlayerLck = 5;      //character's luck stat
    private int PlayerAtk = 0;      //character's attack stat (this is determined by a formula in the Start function of this script
    private int PlayerDef = 0;      //not defined in stats page?
    private float PlayerHitPercent = 10f;       //character's chance to hit an enemy
    private float PlayerEvaPercent = 0f;        //character's chance to dodge an enemy attack
    private int PlayerMDef = 15;            //character's chance to avoid magic attacks (unused at this stage of development)
    private int PlayerStoredAtk = 0;        // used to keep character's attack stat 
    //The Level exp values are static and do not change, these are used to determine the needed exp for leveling up a character
    private int level1Exp = 40;
    private int Level2Exp = 196;
    private int Level3Exp = 547;
    private int Level4Exp = 1171;
    private int LevelMax = 1200;
    private int Level5Cap = 15000;
    //bool used to determine dead state during battles
    private bool Dead = false;
    [SerializeField]
    private int LaneOrder;      //used to determine the lane order in which characters attack in (called in battle script)
    private int ExpToLevel;     //used to determine how much exp is needed in order to level up

    // Use this for initialization
    void Start()
    {
        PlayerNeededExp -= PlayerCurrentExp; //sets the needed exp based on how much exp a character has
        PlayerCurrentHP = PlayerMaxHP; // resets player hp to maximum on start
        myPlayerStats = GetComponent<PlayerStats>(); //reference to self
        //PlayerStoredAtk = PlayerAtk; //sets stored attack equal to 
        PlayerAtk = PlayerStr / 2; //sets player attack based on player's stength
        //PlayerAtk += PlayerStoredAtk;

        if (PlayerLv == 1)
        {
            PlayerNeededExp = level1Exp;
        }
        if (PlayerLv == 2)
        {
            PlayerNeededExp = Level2Exp;
        }
        if (PlayerLv == 3)
        {
            PlayerNeededExp = Level3Exp;
        }
        if (PlayerLv == 4)
        {
            PlayerNeededExp = Level4Exp;
        }

    }

	// Update is called once per frame
	void Update () {

        ExpToLevel = PlayerNeededExp;
        if (Input.GetKeyDown(KeyCode.Space))   //this block of code checks if a character has leveled up and allows testing of exp gain using space bar to increment current exp by 10 points
        {
            PlayerCurrentExp += 10;
        }
        if (PlayerCurrentExp > LevelMax)
        {
            print("Player has reached the current max level");
            PlayerCurrentExp = LevelMax;
        }
        if (PlayerCurrentExp >= PlayerNeededExp)
        {
            print ("Player has leveled up and their stats have increased");
            PlayerLevelUpFunction();
        }
    }

    void PlayerLevelUpFunction() //Players Stats gained per level are currently based off of the 1st 5 levels of the fighter/knight class
        //Function that increments stats based on level
    {
        PlayerLv = PlayerLv + 1;
        if (PlayerLv == 1)
        {
            PlayerNeededExp = level1Exp;
        }
        if (PlayerLv == 2)
        {
            PlayerNeededExp = Level2Exp;
            PlayerStr = PlayerStr + 1;
            PlayerAgl = PlayerAgl + 1;
            PlayerVit = PlayerVit + 1;
            PlayerHitPercent = PlayerHitPercent + 3f;
            PlayerMDef = PlayerMDef + 3;
        }
        if (PlayerLv == 3)
        {
            PlayerNeededExp = Level3Exp;
            PlayerStr = PlayerStr + 1;
            PlayerAgl = PlayerAgl + 1;
            PlayerVit = PlayerVit + 1;
            PlayerLck = PlayerLck + 1;
            PlayerHitPercent = PlayerHitPercent + 3f;
            PlayerMDef = PlayerMDef + 3;
        }
        if (PlayerLv == 4)
        {
            PlayerNeededExp = Level4Exp;
            PlayerStr = PlayerStr + 1;
            PlayerAgl = PlayerAgl + 1;
            PlayerInt = PlayerInt + 1;
            PlayerLck = PlayerLck + 1;
            PlayerHitPercent = PlayerHitPercent + 3f;
            PlayerMDef = PlayerMDef + 3;
        }
        if (PlayerLv == 5)
        {
            PlayerNeededExp = Level5Cap;
            PlayerStr = PlayerStr + 1;
            PlayerAgl = PlayerAgl + 1;
            PlayerVit = PlayerVit + 1;
            PlayerHitPercent = PlayerHitPercent + 3f;
            PlayerMDef = PlayerMDef + 3;
        }
    }
    //public values that can be recieved for other scripts
    //Grabs of values for the pause menu 
    //Can be used to grab values for things such as battle as well
    public string PlayerNameGrab()
    {
        return CharacterName;
    }
    public int PlayerLevelGrab()
    {
        return PlayerLv;
    }
    public int PlayerCurHP
    {
        get { return PlayerCurrentHP; }
        set { PlayerCurrentHP = value; }
    }

    public int PlayerMaxHPGrab()
    {
        return PlayerMaxHP;
    }
    public int PlayerCurExpGrab
    {
        get { return PlayerCurrentExp; }
        set { PlayerCurrentExp = value; }
    }
    public int _CurExpGrab()
    {
        return PlayerCurrentExp;
    }
    public int _ExpToLevel()
    {
        return ExpToLevel;
    }
    public int PlayerNeedExpGrab()
    {
        return PlayerNeededExp;
    }
    public int PlayerStrengthGrab()
    {
        return PlayerStr;
    }
    public int PlayerMagGrab()
    {
        return PlayerMP;
    }
    public int PlayerAgiGrab()
    {
        return PlayerAgl;
    }
    public int PlayerIntGrab()
    {
        return PlayerInt;
    }
    public int PlayerVitGrab()
    {
        return PlayerVit;
    }
    public int PlayerLuckGrab()
    {
        return PlayerLck;
    }
    public int PlayerAtkGrab()
    {
        return PlayerAtk;
    }
    public int PlayerDefGrab()
    {
        return PlayerDef;
    }
    public float PlayerHitGrab()
    {
        return PlayerHitPercent;
    }
    public float PlayerEvaGrab()
    {
        return PlayerEvaPercent;
    }
    public bool DeadGrab
    {
        get { return Dead; }
        set { Dead = value; }
    }
    public int OrderGrab
    {
        get { return LaneOrder; }
        set { LaneOrder = value; }
    }
    
}
