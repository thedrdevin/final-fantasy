﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*-------------------------------------------------------------------------------------------------------------------------------
* Name: Dongwon Yoo
 * Date: 1/1/16
 * Credit: Final Fantasy 1
 * Purpose: This Script is Changing the name indicator according to the monster's name
 * 
 * Edited by Dominic Christiano to Add Wolves/Wargs
 * Edited by Santiago Charry to add Gigas Worm and Black Widow
 --------------------------------------------------------------------------------------------------------------------------*/

public class DY_MonsterName : MonoBehaviour
{
    TextMesh monName2;
    TextMesh monName1;
    DY_MonsterGroup getMonGroup;

    DY_BattleSceneUI UI;

	// Use this for initialization
	void Start ()
    {
        UI = GameObject.Find("UI").GetComponent<DY_BattleSceneUI>();
        getMonGroup = GameObject.Find("MonsterGroup").GetComponent<DY_MonsterGroup>();
        monName2 = GameObject.Find("MonsterID2").GetComponent<TextMesh>();
        monName1 = GameObject.Find("MonsterID").GetComponent<TextMesh>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(UI.BattleUI == 1) // when the DY_BattleSceneUI.BattleSequence is 1, show the names of the monsters
        {
            // the names will be displayed differently by the monsterGroup numbers
            if (getMonGroup.MonsterGroup == 0)
            {
                monName1.text = "IMP";
                monName2.text = "";
            }
            //name of your monster here
            if (getMonGroup.MonsterGroup == 1)
            {
                monName1.text = "Gigas Worm";
                monName2.text = "";
            }
            if (getMonGroup.MonsterGroup == 2)
            {
                monName1.text = "GARLAND";
                monName2.text = "";
            }
            //name of your monster here
            if (getMonGroup.MonsterGroup == 3)
            {
                monName1.text = "Black Widow";
                monName2.text = "";
            }

            if (getMonGroup.MonsterGroup == 4)
            {
                monName1.text = "WARG WOLF";
                monName2.text = "";
            }

            /*if (getMonGroup.MonsterGroup == 4)
            {
                monName1.text = "MonsterNameHere";
                monName2.text = "";
            }*/

        }
        else
        {
            monName1.text = "";
            monName2.text = "";
        }
    }
}
