﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/*-------------------------------------------------------------------------------------------------------------------------------
* Name: Dongwon Yoo
 * Date: 1/1/16
 * Credit: Final Fantasy 1
 * Purpose: This Script is for the Player Action Order
 --------------------------------------------------------------------------------------------------------------------------*/

//This Script is for the Player Action Order
public class DY_TurnOrder : MonoBehaviour
{
    enum Turn { Char1, Char2, Char3, Char4, EnemyTurn, NoTurn }
    Turn curTurn = Turn.Char1;
    Dictionary<Turn, Action> turnRegistry = new Dictionary<Turn, Action>();

    PlayerStats char1HP;
    PlayerStats char2HP;
    PlayerStats char3HP;
    PlayerStats char4HP;
    DR_PlayerRun char1Run;
    DR_PlayerRun char2Run;
    DR_PlayerRun char3Run;
    DR_PlayerRun char4Run;
    MonsterIdentifier mon1;
    MonsterIdentifier mon2;
    MonsterIdentifier mon3;
    MonsterIdentifier mon4;
    MonsterIdentifier mon5;
    MonsterIdentifier mon6;
    MonsterIdentifier mon7;
    MonsterIdentifier mon8;
    MonsterIdentifier mon9;
    DY_BattleSceneUI UI;
    DY_MonsterBehaviour toChar1;

    private GameObject button;
    private GameObject cursor;

    private Animator character1Anim;          // Animator of CharacterHolder#
    private Animator character2Anim;          // Animator of CharacterHolder#
    private Animator character3Anim;          // Animator of CharacterHolder#
    private Animator character4Anim;          // Animator of CharacterHolder#

    [SerializeField] private int turnCount;  // Current Turn if this hits # of survived player, enemy or player will start to attack
    [SerializeField] private int curPlayer;  // this is used for which player is choosing action and getting the variables from the player
    [SerializeField] List<GameObject> players = new List<GameObject>();
    [SerializeField] List<GameObject> forTurnCount = new List<GameObject>();
    [SerializeField] List<GameObject> playersAnim = new List<GameObject>();
    [SerializeField] List<GameObject> characters = new List<GameObject>();

    private bool turnChange; // this will send information of current player has chosen his action
    private bool toEnemyBehave; // if all players have made thier decision, then get into actual battle sequence
    private bool enemyState; // this is turning on/off this script
    private bool cursorandbutton; // this is used for turning on/off the cursor
    private bool go; // when the whole battle scene is over and restarted, this will replace void Start()
    private bool wannaRun; // Detects if the player presses Run Button in the DY_ButtonNavigation script

    private void OnDisable()
    {
        players.RemoveRange(0, players.Count);
        forTurnCount.RemoveRange(0, forTurnCount.Count);
        playersAnim.RemoveRange(0, playersAnim.Count);
        characters.RemoveRange(0, characters.Count);
        turnChange = false;
        toEnemyBehave = false;
        enemyState = false;
        turnCount = 0;
        curPlayer = 0;
        cursorandbutton = false;
        go = false;
        wannaRun = false;
    }

    private void OnEnable()
    {
        SetTurn(Turn.Char1);
        turnCount = 0;
        characters.Add(GameObject.Find("CharacterHolder1"));
        characters.Add(GameObject.Find("CharacterHolder2"));
        characters.Add(GameObject.Find("CharacterHolder3"));
        characters.Add(GameObject.Find("CharacterHolder4"));
        forTurnCount.Add(GameObject.Find("CC1"));
        forTurnCount.Add(GameObject.Find("CC2"));
        forTurnCount.Add(GameObject.Find("CC3"));
        forTurnCount.Add(GameObject.Find("CC4"));
        char1HP = GameObject.Find("CC1").GetComponent<PlayerStats>();
        char2HP = GameObject.Find("CC2").GetComponent<PlayerStats>();
        char3HP = GameObject.Find("CC3").GetComponent<PlayerStats>();
        char4HP = GameObject.Find("CC4").GetComponent<PlayerStats>();
        char1Run = GameObject.Find("CC1").GetComponent<DR_PlayerRun>();
        char2Run = GameObject.Find("CC2").GetComponent<DR_PlayerRun>();
        char3Run = GameObject.Find("CC3").GetComponent<DR_PlayerRun>();
        char4Run = GameObject.Find("CC4").GetComponent<DR_PlayerRun>();
        LaneOrderUpdate();
    }

    // Use this for initialization
    void Start ()
    {
        toChar1 = GameObject.Find("MonsterBehave").GetComponent<DY_MonsterBehaviour>();
        UI = GameObject.Find("UI").GetComponent<DY_BattleSceneUI>();
        button = GameObject.Find("ButtonHolder");
        cursor = GameObject.Find("CursorForBattle");
        char1HP = GameObject.Find("CC1").GetComponent<PlayerStats>();
        char2HP = GameObject.Find("CC2").GetComponent<PlayerStats>();
        char3HP = GameObject.Find("CC3").GetComponent<PlayerStats>();
        char4HP = GameObject.Find("CC4").GetComponent<PlayerStats>();
        mon1 = GameObject.Find("Monster1").GetComponent<MonsterIdentifier>();
        mon2 = GameObject.Find("Monster2").GetComponent<MonsterIdentifier>();
        mon3 = GameObject.Find("Monster3").GetComponent<MonsterIdentifier>();
        mon4 = GameObject.Find("Monster4").GetComponent<MonsterIdentifier>();
        mon5 = GameObject.Find("Monster5").GetComponent<MonsterIdentifier>();
        mon6 = GameObject.Find("Monster6").GetComponent<MonsterIdentifier>();
        mon7 = GameObject.Find("Monster7").GetComponent<MonsterIdentifier>();
        mon8 = GameObject.Find("Monster8").GetComponent<MonsterIdentifier>();
        mon9 = GameObject.Find("Monster9").GetComponent<MonsterIdentifier>();
        turnRegistry.Add(Turn.Char1, new Action(Char1));
        turnRegistry.Add(Turn.Char2, new Action(Char2));
        turnRegistry.Add(Turn.Char3, new Action(Char3));
        turnRegistry.Add(Turn.Char4, new Action(Char4));
        turnRegistry.Add(Turn.EnemyTurn, new Action(EnemyTurn));
        turnRegistry.Add(Turn.NoTurn, new Action(NoTurn));
        cursor.SetActive(false);
        button.SetActive(false);
        UI.BattleUI = 0;
        turnCount = 0;
        LaneOrderUpdate();
    }

    // Update is called once per frame
    void Update()
    {
        turnRegistry[curTurn].Invoke();

        // all animation resets
        if (!go)
        {
            character1Anim = GameObject.Find("CharacterHolder1").GetComponent<Animator>();
            character2Anim = GameObject.Find("CharacterHolder2").GetComponent<Animator>();
            character3Anim = GameObject.Find("CharacterHolder3").GetComponent<Animator>();
            character4Anim = GameObject.Find("CharacterHolder4").GetComponent<Animator>();
            character1Anim.CrossFadeInFixedTime("DY_Idle", 0);
            character2Anim.CrossFadeInFixedTime("DY_Idle", 0);
            character3Anim.CrossFadeInFixedTime("DY_Idle", 0);
            character4Anim.CrossFadeInFixedTime("DY_Idle", 0);
            go = true;
        }

        if (!toChar1.RunSuccess) // has run success?
        {
            if (curTurn == Turn.Char1)
            {
                Char1();
            }
            else if (curTurn == Turn.Char2)
            {
                Char2();
            }
            else if (curTurn == Turn.Char3)
            {
                Char3();
            }
            else if (curTurn == Turn.Char4)
            {
                Char4();
            }
            else if (curTurn == Turn.EnemyTurn)
            {
                EnemyTurn();
            }
            else if(curTurn == Turn.NoTurn)
            {
                NoTurn();
            }
        }
        else // if run success, battle is over
        {
            cursor.SetActive(false);
            button.SetActive(false);
            UI.BattleUI = 4;
            SetTurn(Turn.NoTurn);
        }

        if (!cursorandbutton) // if battle is over, hide the cursor and buttons
        {
            cursor.SetActive(false);
            button.SetActive(false);
            cursorandbutton = true;
        }

        if (toChar1.BattleWin) // if battle has finished by winning, hide the cursor and buttons
        {
            cursor.SetActive(false);
            button.SetActive(false);
            UI.BattleUI = 3;
            SetTurn(Turn.NoTurn);
        }
        else if (toChar1.LevelUp) // if character level up, hide the cursor and buttons
        {
            cursor.SetActive(false);
            button.SetActive(false);
            UI.BattleUI = 6;
            SetTurn(Turn.NoTurn);
        }
        else if (toChar1.GameOver) // if battle has finished by losing, hide the cursor and buttons
        {
            cursor.SetActive(false);
            button.SetActive(false);
            UI.BattleUI = 4;
            SetTurn(Turn.NoTurn);
        }

        if (char1HP.PlayerCurHP <= 0) // if player is dead, remove that player from the list to reduce the max turn count
        {
            forTurnCount.Remove(GameObject.Find("CC1"));
        }
        if (char2HP.PlayerCurHP <= 0)
        {
            forTurnCount.Remove(GameObject.Find("CC2"));
        }
        if (char3HP.PlayerCurHP <= 0)
        {
            forTurnCount.Remove(GameObject.Find("CC3"));
        }
        if (char4HP.PlayerCurHP <= 0)
        {
            forTurnCount.Remove(GameObject.Find("CC4"));
        }
    }

    void LaneOrderUpdate() // according to lane order number that is attached to CC#, reorder the list of players. this is affecting the curPlayer(CurrentPlayer)
    {
        players.RemoveRange(0, players.Count);
        playersAnim.RemoveRange(0, playersAnim.Count);

        if(char1HP.OrderGrab == 1)
        {
            characters[0].transform.position = new Vector2(2.5f, 2.5f);
            players.Add(GameObject.Find("CC1"));
            playersAnim.Add(GameObject.Find("CharacterHolder1"));
        }
        else if (char2HP.OrderGrab == 1)
        {
            characters[1].transform.position = new Vector2(2.5f, 2.5f);
            players.Add(GameObject.Find("CC2"));
            playersAnim.Add(GameObject.Find("CharacterHolder2"));
        }
        else if (char3HP.OrderGrab == 1)
        {
            characters[2].transform.position = new Vector2(2.5f, 2.5f);
            players.Add(GameObject.Find("CC3"));
            playersAnim.Add(GameObject.Find("CharacterHolder3"));
        }
        else if (char4HP.OrderGrab == 1)
        {
            characters[3].transform.position = new Vector2(2.5f, 2.5f);
            players.Add(GameObject.Find("CC4"));
            playersAnim.Add(GameObject.Find("CharacterHolder4"));
        }

        if (char1HP.OrderGrab == 2)
        {
            characters[0].transform.position = new Vector2(2.5f, 1.4f);
            players.Add(GameObject.Find("CC1"));
            playersAnim.Add(GameObject.Find("CharacterHolder1"));
        }
        else if (char2HP.OrderGrab == 2)
        {
            characters[1].transform.position = new Vector2(2.5f, 1.4f);
            players.Add(GameObject.Find("CC2"));
            playersAnim.Add(GameObject.Find("CharacterHolder2"));
        }
        else if (char3HP.OrderGrab == 2)
        {
            characters[2].transform.position = new Vector2(2.5f, 1.4f);
            players.Add(GameObject.Find("CC3"));
            playersAnim.Add(GameObject.Find("CharacterHolder3"));
        }
        else if (char4HP.OrderGrab == 2)
        {
            characters[3].transform.position = new Vector2(2.5f, 1.4f);
            players.Add(GameObject.Find("CC4"));
            playersAnim.Add(GameObject.Find("CharacterHolder4"));
        }

        if (char1HP.OrderGrab == 3)
        {
            characters[0].transform.position = new Vector2(2.5f, 0.3f);
            players.Add(GameObject.Find("CC1"));
            playersAnim.Add(GameObject.Find("CharacterHolder1"));
        }
        else if (char2HP.OrderGrab == 3)
        {
            characters[1].transform.position = new Vector2(2.5f, 0.3f);
            players.Add(GameObject.Find("CC2"));
            playersAnim.Add(GameObject.Find("CharacterHolder2"));
        }
        else if (char3HP.OrderGrab == 3)
        {
            characters[2].transform.position = new Vector2(2.5f, 0.3f);
            players.Add(GameObject.Find("CC3"));
            playersAnim.Add(GameObject.Find("CharacterHolder3"));
        }
        else if (char4HP.OrderGrab == 3)
        {
            characters[3].transform.position = new Vector2(2.5f, 0.3f);
            players.Add(GameObject.Find("CC4"));
            playersAnim.Add(GameObject.Find("CharacterHolder4"));
        }

        if (char1HP.OrderGrab == 4)
        {
            characters[0].transform.position = new Vector2(2.5f, -0.8f);
            players.Add(GameObject.Find("CC1"));
            playersAnim.Add(GameObject.Find("CharacterHolder1"));
        }
        else if (char2HP.OrderGrab == 4)
        {
            characters[1].transform.position = new Vector2(2.5f, -0.8f);
            players.Add(GameObject.Find("CC2"));
            playersAnim.Add(GameObject.Find("CharacterHolder2"));
        }
        else if (char3HP.OrderGrab == 4)
        {
            characters[2].transform.position = new Vector2(2.5f, -0.8f);
            players.Add(GameObject.Find("CC3"));
            playersAnim.Add(GameObject.Find("CharacterHolder3"));
        }
        else if (char4HP.OrderGrab == 4)
        {
            characters[3].transform.position = new Vector2(2.5f, -0.8f);
            players.Add(GameObject.Find("CC4"));
            playersAnim.Add(GameObject.Find("CharacterHolder4"));
        }
    }

    void SetTurn(Turn newTurn)
    {
        curTurn = newTurn;
    }

    public void TurnOver() // if all players have made their decision, circle again. if not, go to battle system(DY_MonsterBehaviour).
    {
        if (forTurnCount.Count - 1 == turnCount)
        {
            if (curTurn == Turn.Char1 && !turnChange)
            {
                CharacterGoBack();
                SetTurn(Turn.Char2);
            }
            else if (curTurn == Turn.Char2 && !turnChange)
            {
                CharacterGoBack();
                SetTurn(Turn.Char3);
            }
            else if (curTurn == Turn.Char3 && !turnChange)
            {
                CharacterGoBack();
                SetTurn(Turn.Char4);
            }
            else if (curTurn == Turn.Char4 && !turnChange)
            {
                CharacterGoBack();
                SetTurn(Turn.Char1);
            }
            if (wannaRun)
            {
                players[curPlayer].GetComponent<DR_PlayerRun>().Run = true;
                wannaRun = false;
            }
            SetTurn(Turn.EnemyTurn);
            turnCount = 0;
        }
        else if(forTurnCount.Count > turnCount)
        {
            if (curTurn == Turn.Char1 && !turnChange)
            {
                CharacterGoBack();
                SetTurn(Turn.Char2);
            }
            else if (curTurn == Turn.Char2 && !turnChange)
            {
                CharacterGoBack();
                SetTurn(Turn.Char3);
            }
            else if (curTurn == Turn.Char3 && !turnChange)
            {
                CharacterGoBack();
                SetTurn(Turn.Char4);
            }
            else if (curTurn == Turn.Char4 && !turnChange)
            {
                CharacterGoBack();
                SetTurn(Turn.Char1);
            }
            if (wannaRun)
            {
                players[curPlayer].GetComponent<DR_PlayerRun>().Run = true;
                wannaRun = false;
            }
            turnCount++;
        }
        turnChange = true;
        TurnChange();
    }

    void Char1()
    {
        curPlayer = 0;
        if (players[curPlayer].GetComponent<PlayerStats>().PlayerCurHP > 0) // if current player is not dead
        {
            CharacterAdvance(); // character1 forward
            if (playersAnim[curPlayer].GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("DY_IdleForward")) // if character1 is ahead
            {
                UI.BattleUI = 1; // change the background image
                cursor.SetActive(true); // cursor appear
                button.SetActive(true); // button appear
            }
        }
        else // if current player is dead go to turn 2
        {
            SetTurn(Turn.Char2);
        }
    }

    void Char2()
    {
        curPlayer = 1;
        if (players[curPlayer].GetComponent<PlayerStats>().PlayerCurHP > 0)
        {
            CharacterAdvance();
            if (playersAnim[curPlayer].GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("DY_IdleForward"))
            {
                UI.BattleUI = 1;
                cursor.SetActive(true);
                button.SetActive(true);
            }
        }
        else
        {
            SetTurn(Turn.Char3);
        }
    }

    void Char3()
    {
        curPlayer = 2;
        if (players[curPlayer].GetComponent<PlayerStats>().PlayerCurHP > 0)
        {
            CharacterAdvance();
            if (playersAnim[curPlayer].GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("DY_IdleForward"))
            {
                UI.BattleUI = 1;
                cursor.SetActive(true);
                button.SetActive(true);
            }
        }
        else
        {
            SetTurn(Turn.Char4);
        }
    }

    void Char4()
    {
        curPlayer = 3;
        if (players[curPlayer].GetComponent<PlayerStats>().PlayerCurHP > 0)
        {
            CharacterAdvance();
            if (playersAnim[curPlayer].GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("DY_IdleForward"))
            {
                UI.BattleUI = 1;
                cursor.SetActive(true);
                button.SetActive(true);
            }
        }
        else
        {
            SetTurn(Turn.Char1);
        }
    }

    void EnemyTurn()
    {
        if (!enemyState)
        {
            if (playersAnim[curPlayer].GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("DY_Idle"))
            {
                toEnemyBehave = true;
                enemyState = true;
            } 
        }
        else if (toChar1.TurnOver)
        {
            toChar1.TurnOver = false;
            enemyState = false;
            SetTurn(Turn.Char1);
        }
    }

    void NoTurn()
    {

    }

    void CharacterAdvance()
    {
        playersAnim[curPlayer].GetComponent<Animator>().SetBool("Go", true);
        playersAnim[curPlayer].GetComponent<Animator>().SetBool("Back", false);
    }

    void CharacterGoBack()
    {
        playersAnim[curPlayer].GetComponent<Animator>().SetBool("Go", false);
        playersAnim[curPlayer].GetComponent<Animator>().SetBool("Back", true);
        UI.BattleUI = 0;
        cursor.SetActive(false);
        button.SetActive(false);
    }

    void TurnChange()
    {
        turnChange = false;
    }

    public bool EnemyBehaveSwitch
    {
        get
        {
            return toEnemyBehave;
        }
        set
        {
            toEnemyBehave = value;
        }
    }

    public bool WannaRun { get { return wannaRun; } set { wannaRun = value; } }
}
