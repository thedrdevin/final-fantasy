﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelroyStartOver : MonoBehaviour
    /* Code By: Delroy Gaskin
     * Purpose: This Script causes the enemies to refresh. This refresh stops the game from spawning the same enemies at the start.
     */
{

    MonsterIdentifier mon1;
    MonsterIdentifier mon2;
    MonsterIdentifier mon3;
    MonsterIdentifier mon4;
    MonsterIdentifier mon5;
    MonsterIdentifier mon6;
    MonsterIdentifier mon7;
    MonsterIdentifier mon8;
    MonsterIdentifier mon9;

    [SerializeField] bool refresh;

    // Use this for initialization
    void Start ()
    {
        mon1 = GameObject.Find("Monster1").GetComponent<MonsterIdentifier>();
        mon2 = GameObject.Find("Monster2").GetComponent<MonsterIdentifier>();
        mon3 = GameObject.Find("Monster3").GetComponent<MonsterIdentifier>();
        mon4 = GameObject.Find("Monster4").GetComponent<MonsterIdentifier>();
        mon5 = GameObject.Find("Monster5").GetComponent<MonsterIdentifier>();
        mon6 = GameObject.Find("Monster6").GetComponent<MonsterIdentifier>();
        mon7 = GameObject.Find("Monster7").GetComponent<MonsterIdentifier>();
        mon8 = GameObject.Find("Monster8").GetComponent<MonsterIdentifier>();
        mon9 = GameObject.Find("Monster9").GetComponent<MonsterIdentifier>();

    }

    private void OnEnable()
    {
        refresh = true;
        Debug.Log("StartOver");
    }

    // Update is called once per frame
    void Update ()
    {
        if (refresh)
        {
            mon1.StartOver = true;
            mon2.StartOver = true;
            mon3.StartOver = true;
            mon4.StartOver = true;
            mon5.StartOver = true;
            mon6.StartOver = true;
            mon7.StartOver = true;
            mon8.StartOver = true;
            mon9.StartOver = true;
            refresh = false;

        }
    }
}
