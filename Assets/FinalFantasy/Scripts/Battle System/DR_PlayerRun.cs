﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 /*-------------------------------------------------------------------------------------------------------------------------------
 * Name: Devin Rhoads
 * Date: 5/15/18
 * Credit: Final Fantasy 1
 * Purpose: This script is pretty much here to talk to MonsterBehavior about who is trying to run.  
 -------------------------------------------------------------------------------------------------------------------------------*/

public class DR_PlayerRun : MonoBehaviour {

    public bool run;
    public int target;

	// Use this for initialization
	void Start ()
    {
        run = false;
        target = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public bool Run {  get { return run; } set { run = value; } }
    public int Target { get { return target; } set { target = value; } }
}
