﻿using System.IO;
using System.Collections.Generic;


//Author: Dongwon Yoo
//Credit: Unsung Yoo
/*-------------------------------------------------------------------------------------------------------------------------------
* Name: Dongwon Yoo
 * Date: 1/1/16
 * Credit: Final Fantasy 1
 * Purpose: This Script is for reading the MonsterData.txt file.
 --------------------------------------------------------------------------------------------------------------------------*/

public class Struct_MonsterData
{
    public int monsterNum; // this does nothing. this is for convenience for the developer.
    public string name; // name of the monster
    public int hp; // maxHP
    public int atk; // attak point of the monster
    public int acc; // accuracy of the monster
    public int crt; // critical of the monster
    public int def; // defnse of the monster
    public int eva; // evasion of the monster
    public int md; // magic defense of the monster
    public int mor; //morale of the monster
    public int magic; // this does nothing. this is for convenience for the developer.
    public int special; // this does nothing. this is for convenience for the developer.
    public int gold; // gold of the monster
    public int exp; // exp of the monster
}


//Reading the MonsterData.txt file
public class MonsterData
{
    public List<Struct_MonsterData> monsterData;

    public MonsterData()
    {
        MonsterData monsterFileRead = new MonsterData();
        monsterData = ReadData("./Assets/MonsterData.txt");
    }

    public List<Struct_MonsterData> ReadData(string filepath)
    {
        List<Struct_MonsterData> result = new List<Struct_MonsterData>();
        using (StreamReader sr = File.OpenText(filepath))
        {
            string s = "";
            while ((s = sr.ReadLine()) != null)
            {
                string[] strs;
                strs = s.Split('/');
                Struct_MonsterData newMonster = new Struct_MonsterData();
                newMonster.monsterNum = int.Parse(strs[0]);
                newMonster.name = strs[1];
                newMonster.hp = int.Parse(strs[2]);
                newMonster.atk = int.Parse(strs[3]);
                newMonster.acc = int.Parse(strs[4]);
                newMonster.crt = int.Parse(strs[5]);
                newMonster.def = int.Parse(strs[6]);
                newMonster.eva = int.Parse(strs[7]);
                newMonster.md = int.Parse(strs[8]);
                newMonster.mor = int.Parse(strs[9]);
                newMonster.magic = int.Parse(strs[10]);
                newMonster.special = int.Parse(strs[11]);
                newMonster.gold = int.Parse(strs[12]);
                newMonster.exp = int.Parse(strs[13]);
                result.Add(newMonster);
            }
        }

        return result;
    }

    public static Struct_MonsterData ReadData(string filepath, int monsterType)
    {
        using (StreamReader sr = File.OpenText(filepath))
        {
            string s = "";
            for (int i = 0; (s = sr.ReadLine()) != null; i++)
            {
                if (i != monsterType)
                    continue;

                string[] strs;
                strs = s.Split('/');
                Struct_MonsterData newMonster = new Struct_MonsterData();
                newMonster.monsterNum = int.Parse(strs[0]);
                newMonster.name = strs[1];
                newMonster.hp = int.Parse(strs[2]);
                newMonster.atk = int.Parse(strs[3]);
                newMonster.acc = int.Parse(strs[4]);
                newMonster.crt = int.Parse(strs[5]);
                newMonster.def = int.Parse(strs[6]);
                newMonster.eva = int.Parse(strs[7]);
                newMonster.md = int.Parse(strs[8]);
                newMonster.mor = int.Parse(strs[9]);
                newMonster.magic = int.Parse(strs[10]);
                newMonster.special = int.Parse(strs[11]);
                newMonster.gold = int.Parse(strs[12]);
                newMonster.exp = int.Parse(strs[13]);

                return newMonster;
            }
        }

        return new Struct_MonsterData();
    }
}
