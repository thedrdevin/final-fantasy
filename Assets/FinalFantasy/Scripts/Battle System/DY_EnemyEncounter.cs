﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DY_EnemyEncounter : MonoBehaviour
{
    [SerializeField] GameObject overWorld;
    [SerializeField] GameObject battle;
    [SerializeField] GameObject o_MonBehave;
    [SerializeField] TransistionGlobal transition;
    DY_MonsterBehaviour monBehave;
    float timer;
    bool timerStart;

    // Use this for initialization
    void Start ()
    {
        monBehave = o_MonBehave.GetComponent<DY_MonsterBehaviour>();
        transition = GameObject.Find("SceneController").GetComponent<TransistionGlobal>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (monBehave.BattleEnd)
        {
            TimerStart();
            if (monBehave.LevelUp)
            {
                if (timer > 4)
                {
                    transition.BattleToOverworld();
                    timer = 0;
                }
            }
            else if (monBehave.GameOver)
            {
                if (timer > 3)
                {
                    transition.BattleToOverworld();
                    timer = 0;
                }
            }
            else if(!monBehave.GameOver)
            {
                if (timer > 3)
                {
                    transition.BattleToOverworld();
                    timer = 0;
                }
            }
        }
	}

    public void EnemyEncounter()
    {
        transition.OverworldToBattle();
    }

    void TimerStart()
    {
        timer += Time.deltaTime;
    }
}
