﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/*-------------------------------------------------------------------------------------------------------------------------------
* Name: Dongwon Yoo
 * Date: 1/1/16
 * Credit: Final Fantasy 1
 * Purpose: This Script is getting the monster's stat from the MonsterData.txt and MonsterData.cs files.
 * Edited by Devin Rhoads: The game now checks to see which MonIdentity is active and turns on the group of monsters with 
 * those correct sprites.  
 --------------------------------------------------------------------------------------------------------------------------*/

public class MonsterIdentifier : MonoBehaviour
{

    [SerializeField] private int monIdentity;
    [SerializeField] private int monsterNum;
    [SerializeField] private string monName;
    [SerializeField] private int maxHP;
    [SerializeField] private int currentHP;
    [SerializeField] private int atk;
    [SerializeField] private int acc;
    [SerializeField] private int crt;
    [SerializeField] private int def;
    [SerializeField] private int eva;
    [SerializeField] private int md;
    [SerializeField] private int mor;
    [SerializeField] private int magic;
    [SerializeField] private int special;
    [SerializeField] private int gold;
    [SerializeField] private int exp;

    [SerializeField] private bool dead;
    [SerializeField] private bool run;

    private SpriteRenderer spr;

    public Sprite image;

    [SerializeField] private bool startOver;
    DY_MonsterGroup monsterGroup;

    //For turning on and off the correct enemy sprites
    public GameObject mon0;
    public GameObject mon1;
    public GameObject mon2;
    public GameObject mon3;
    public GameObject mon4;
    public GameObject mon5;
    public GameObject mon6;

    private void OnEnable()
    {
        Start();
    }

    // Use this for initialization
    void Start()
    {
        //MonsterData monster = new MonsterData();
        //Struct_MonsterData data = monster.monsterData[monIdentity];
        //Debug.Log(monName);
        Struct_MonsterData data = MonsterData.ReadData("./Assets/MonsterData.txt", monIdentity);

        spr = GetComponent<SpriteRenderer>();
        monsterGroup = GameObject.Find("MonsterGroup").GetComponent<DY_MonsterGroup>();

        monsterNum = data.monsterNum;
        monName = data.name;
        maxHP = data.hp;
        currentHP = maxHP;
        atk = data.atk;
        acc = data.acc;
        crt = data.crt;
        def = data.def;
        eva = data.eva;
        md = data.md;
        mor = data.mor;
        magic = data.magic;
        special = data.special;
        gold = data.gold;
        exp = data.exp;

        //currentHP = maxHP;

    }

    // Update is called once per frame
    void Update()
    {
        
        if (currentHP <= 0 || dead)
        {
            mon0.SetActive(false);
            mon1.SetActive(false);
            mon2.SetActive(false);
            mon3.SetActive(false);
            mon4.SetActive(false);
            mon5.SetActive(false);
            mon6.SetActive(false);
            dead = true;
        }
        else
        {
            if (monIdentity == 0)
            {
                //monName = ("Imp");
                //generates an random number to see which wolf will be fought
                int diffEnemy = UnityEngine.Random.Range(1, 10);
                //sets the normal imp as active
                if( diffEnemy <= 5)
                {
                    mon0.SetActive(true);
                    mon1.SetActive(false);
                    mon2.SetActive(false);
                    mon3.SetActive(false);
                    mon4.SetActive(false);
                    mon5.SetActive(false);
                    mon6.SetActive(false);
                }
                //sets the ice imp as active
                if (diffEnemy >= 6)
                {
                    mon0.SetActive(false);
                    mon1.SetActive(false);
                    mon2.SetActive(false);
                    mon3.SetActive(false);
                    mon4.SetActive(false);
                    mon5.SetActive(true);
                    mon6.SetActive(false);
                }

            }
            if (monIdentity == 1)
            {
                //monName = ("Gigas Worm");

                mon0.SetActive(false);
                mon1.SetActive(true);
                mon2.SetActive(false);
                mon3.SetActive(false);
                mon4.SetActive(false);
                mon5.SetActive(false);
                mon6.SetActive(false);
            }
            if (monIdentity == 2)
            {
                //monName = ("Black Widow");

                mon0.SetActive(false);
                mon1.SetActive(false);
                mon2.SetActive(false);
                mon3.SetActive(true);
                mon4.SetActive(false);
                mon5.SetActive(false);
                mon6.SetActive(false);
            }
            if (monIdentity == 3)
            {
                //monName = ("Black Widow");

                mon0.SetActive(false);
                mon1.SetActive(false);
                mon2.SetActive(false);
                mon3.SetActive(true);
                mon4.SetActive(false);
                mon5.SetActive(false);
                mon6.SetActive(false);
            }
            if (monIdentity == 4)
            {
                //generates an random number to see which wolf will be fought
                int diffEnemy = UnityEngine.Random.Range(1, 10);
                //sets the normal wolf to active
                if (diffEnemy <= 5)
                {
                    mon0.SetActive(false);
                    mon1.SetActive(false);
                    mon2.SetActive(false);
                    mon3.SetActive(false);
                    mon4.SetActive(true);
                    mon5.SetActive(false);
                    mon6.SetActive(false);
                }
                //sets the ice wolf to active
                if (diffEnemy >= 6)
                {
                    mon0.SetActive(false);
                    mon1.SetActive(false);
                    mon2.SetActive(false);
                    mon3.SetActive(false);
                    mon4.SetActive(false);
                    mon5.SetActive(false);
                    mon6.SetActive(true);
                }
            }
        }

        if (startOver)
        {
            Start();
            startOver = false;
        }

    }

    
    public int MonsterIdentity
    {
        get
        {
            return monIdentity;
        }
        set
        {
            monIdentity = value;
        }
    }

    public bool Dead
    {
        get
        {
            return dead;
        }
        set
        {
            dead = value;
        }
    }

    public bool Run { get { return run; } set { run = value; } }

    public string MonsterName { get { return monName; } }

    public int MOR { get { return mor; } }

    public int ATK { get { return atk; } }

    public int DEF { get { return def; } }

    public int CurrentHP { get { return currentHP; } set { currentHP = value; } }

    public int ACC { get { return acc; } set { acc = value; } }

    public int EVA { get { return eva; } set { eva = value; } }

    public int GOLD { get { return gold; } set { gold = value; } }

    public int EXP { get { return exp; } set { exp = value; } }

    public bool StartOver { get { return startOver; } set { startOver = value; } }
}
