﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/*-------------------------------------------------------------------------------------------------------------------------------
* Name: Dongwon Yoo
 * Date: 1/1/16
 * Credit: Final Fantasy 1
 * Purpose: his Script is the Core System for the Battle Function
 * Edited by Devin Rhoads: add in critical hits, running from battle, and small fixes
 * 
 --------------------------------------------------------------------------------------------------------------------------*/

// This Script is the Core System for the Battle Function
public class DY_MonsterBehaviour : MonoBehaviour
{
    PlayerStats playerStat1;
    PlayerStats playerStat2;
    PlayerStats playerStat3;
    PlayerStats playerStat4;
    MonsterIdentifier mon1;
    MonsterIdentifier mon2;
    MonsterIdentifier mon3;
    MonsterIdentifier mon4;
    MonsterIdentifier mon5;
    MonsterIdentifier mon6;
    MonsterIdentifier mon7;
    MonsterIdentifier mon8;
    MonsterIdentifier mon9;
    DY_MonsterGroup monGroup;
    DY_BattleSceneUI UI;
    DY_TurnOrder turnScript;
    //[SerializeField] GameObject o_inventory;
    //Inventory inventory;

    [SerializeField] List<GameObject> turnOrder = new List<GameObject>();
    [SerializeField] List<GameObject> players = new List<GameObject>();
    [SerializeField] List<GameObject> monsters = new List<GameObject>();
    [SerializeField] private int tempTargetedMonster;            //this holds temporary variable for which monster has been chosen to be attacked
    [SerializeField] private int monChosen;                      //this counts how many player has chosen monsters to attack
    [SerializeField] private int tempGold;                       //this holds temporary variable for how much gold has been accumulated
    [SerializeField] private int tempEXP;                        //this holds temporary variable for how much exp has been accumulated
    [SerializeField] private int curTurn;                        //this tells who is taking an action
    private float timer;
    private float reachPoint;
    private int targetedPlayer;             // monster decided to attack this player
    private int monstersKilled;             // how many monsters are killed
    private int goGo;                       // this is the number of monsters when the battle calculation is going on
    private bool turnOver;                  // battle calculation and system are done, go back to player choosing action (DY_TurnOrder)
    private bool runSuccess;                // player's runn attempt succeeded
    private bool gameOver;                  // player lost
    private bool battleWin;                 // player win
    private bool levelUP;                   // player level up after winning
    private bool battleEnd;
    private GameObject tempGO;             
    private GameObject tempGO2;             
    private int player1maxEXP;              // player's max EXP. this is temporary
    private int player2maxEXP;              // player's max EXP. this is temporary
    private int player3maxEXP;              // player's max EXP. this is temporary
    private int player4maxEXP;              // player's max EXP. this is temporary
    private int i = 0;                      // which player's level up displayer has been shown?

    TextMesh attacker;          //text for indicators after battle is finished(Lev. UP) / text for whos attacking
    TextMesh defender;          //text for indicators after battle is finished(HP max) / text for whos defending
    TextMesh damageAmount;      //text for indicators after battle is finished(gold) / text for how much damage dealt
    TextMesh EXPdis;            //text for indicators after battle is finished(exp)
    TextMesh loseText;          //text for indicators after battle is finished(lose)

    private void OnDisable()
    {
        turnOrder.RemoveRange(0, turnOrder.Count);
        players.RemoveRange(0, players.Count);
        monsters.RemoveRange(0, monsters.Count);
        tempTargetedMonster = 0;
        monChosen = 0;
        goGo = 0;
        tempGold = 0;
        tempEXP = 0;
        curTurn = 0;
        timer = 0;
        reachPoint = 0;
        targetedPlayer = 0;
        monstersKilled = 0;
        turnOver = false;
        runSuccess = false;
        gameOver = false;
        battleWin = false;
        levelUP = false;
        battleEnd = false;
        player1maxEXP = 0;
        player2maxEXP = 0;
        player3maxEXP = 0;
        player4maxEXP = 0;
        i = 0;
        loseText.text = "";
    }

    private void OnEnable()
    {
        monsters.Add(GameObject.Find("Monster1"));
        monsters.Add(GameObject.Find("Monster2"));
        monsters.Add(GameObject.Find("Monster3"));
        monsters.Add(GameObject.Find("Monster4"));
        monsters.Add(GameObject.Find("Monster5"));
        monsters.Add(GameObject.Find("Monster6"));
        monsters.Add(GameObject.Find("Monster7"));
        monsters.Add(GameObject.Find("Monster8"));
        monsters.Add(GameObject.Find("Monster9"));
        turnOrder.Add(GameObject.Find("Monster1"));
        turnOrder.Add(GameObject.Find("Monster2"));
        turnOrder.Add(GameObject.Find("Monster3"));
        turnOrder.Add(GameObject.Find("Monster4"));
        turnOrder.Add(GameObject.Find("Monster5"));
        turnOrder.Add(GameObject.Find("Monster6"));
        turnOrder.Add(GameObject.Find("Monster7"));
        turnOrder.Add(GameObject.Find("Monster8"));
        turnOrder.Add(GameObject.Find("Monster9"));
        turnOrder.Add(GameObject.Find("CC1"));
        turnOrder.Add(GameObject.Find("CC2"));
        turnOrder.Add(GameObject.Find("CC3"));
        turnOrder.Add(GameObject.Find("CC4"));
        Start();
    }

    // Use this for initialization
    void Start ()
    {
        //inventory = o_inventory.GetComponent<Inventory>();
        loseText = GameObject.Find("LoseText").GetComponent<TextMesh>();
        EXPdis = GameObject.Find("EXP").GetComponent<TextMesh>();
        monGroup = GameObject.Find("MonsterGroup").GetComponent<DY_MonsterGroup>();
        attacker = GameObject.Find("IDforAttacker").GetComponent<TextMesh>();
        defender = GameObject.Find("IDforDefender").GetComponent<TextMesh>();
        damageAmount = GameObject.Find("DamageAmount").GetComponent<TextMesh>();
        UI = GameObject.Find("UI").GetComponent<DY_BattleSceneUI>();
        playerStat1 = GameObject.Find("CC1").GetComponent<PlayerStats>();
        playerStat2 = GameObject.Find("CC2").GetComponent<PlayerStats>();
        playerStat3 = GameObject.Find("CC3").GetComponent<PlayerStats>();
        playerStat4 = GameObject.Find("CC4").GetComponent<PlayerStats>();
        mon1 = GameObject.Find("Monster1").GetComponent<MonsterIdentifier>();
        mon2 = GameObject.Find("Monster2").GetComponent<MonsterIdentifier>();
        mon3 = GameObject.Find("Monster3").GetComponent<MonsterIdentifier>();
        mon4 = GameObject.Find("Monster4").GetComponent<MonsterIdentifier>();
        mon5 = GameObject.Find("Monster5").GetComponent<MonsterIdentifier>();
        mon6 = GameObject.Find("Monster6").GetComponent<MonsterIdentifier>();
        mon7 = GameObject.Find("Monster7").GetComponent<MonsterIdentifier>();
        mon8 = GameObject.Find("Monster8").GetComponent<MonsterIdentifier>();
        mon9 = GameObject.Find("Monster9").GetComponent<MonsterIdentifier>();
        turnScript = GameObject.Find("TurnOrder").GetComponent<DY_TurnOrder>();
        reachPoint = 1.5f;
        TurnOrderRandom();
        attacker.text = "";
        defender.text = "";
        damageAmount.text = "";
        playerStat1.PlayerCurHP = playerStat1.PlayerMaxHPGrab();
        playerStat2.PlayerCurHP = playerStat2.PlayerMaxHPGrab();
        playerStat3.PlayerCurHP = playerStat3.PlayerMaxHPGrab();
        playerStat4.PlayerCurHP = playerStat4.PlayerMaxHPGrab();
        LaneOrderUpdate();
        player1maxEXP = players[0].GetComponent<PlayerStats>().PlayerNeedExpGrab();
        player2maxEXP = players[1].GetComponent<PlayerStats>().PlayerNeedExpGrab();
        player3maxEXP = players[2].GetComponent<PlayerStats>().PlayerNeedExpGrab();
        player4maxEXP = players[3].GetComponent<PlayerStats>().PlayerNeedExpGrab();
    }

    // Update is called once per frame
    void Update ()
    {
        timer += Time.deltaTime;

        //checks if someone's dead. this is affecting how much characters are attacking or defending
        if (playerStat1.PlayerCurHP <= 0)
        {
            turnOrder.Remove(GameObject.Find("CC1"));
        }
        if (playerStat2.PlayerCurHP <= 0)
        {
            turnOrder.Remove(GameObject.Find("CC2"));
        }
        if (playerStat3.PlayerCurHP <= 0)
        {
            turnOrder.Remove(GameObject.Find("CC3"));
        }
        if (playerStat4.PlayerCurHP <= 0)
        {
            turnOrder.Remove(GameObject.Find("CC4"));
        }
        if (mon1.Dead)
        {
            turnOrder.Remove(GameObject.Find("Monster1"));
        }
        if (mon2.Dead)
        {
            turnOrder.Remove(GameObject.Find("Monster2"));
        }
        if (mon3.Dead)
        {
            turnOrder.Remove(GameObject.Find("Monster3"));
        }
        if (mon4.Dead)
        {
            turnOrder.Remove(GameObject.Find("Monster4"));
        }
        if (mon5.Dead)
        {
            turnOrder.Remove(GameObject.Find("Monster5"));
        }
        if (mon6.Dead)
        {
            turnOrder.Remove(GameObject.Find("Monster6"));
        }
        if (mon7.Dead)
        {
            turnOrder.Remove(GameObject.Find("Monster7"));
        }
        if (mon8.Dead)
        {
            turnOrder.Remove(GameObject.Find("Monster8"));
        }
        if (mon9.Dead)
        {
            turnOrder.Remove(GameObject.Find("Monster9"));
        }

        //When the monster dies, this if group saves the gold and exp to the temp variables
        if(mon1.CurrentHP <= 0)
        {
            mon1.CurrentHP = 999;
            tempEXP = tempEXP + mon1.EXP;
            tempGold = tempGold + mon1.GOLD;
            monstersKilled++;
        }
        if (mon2.CurrentHP <= 0)
        {
            mon2.CurrentHP = 999;
            tempEXP = tempEXP + mon2.EXP;
            tempGold = tempGold + mon2.GOLD;
            monstersKilled++;
        }
        if (mon3.CurrentHP <= 0)
        {
            mon3.CurrentHP = 999;
            tempEXP = tempEXP + mon3.EXP;
            tempGold = tempGold + mon3.GOLD;
            monstersKilled++;
        }
        if (mon4.CurrentHP <= 0)
        {
            mon4.CurrentHP = 999;
            tempEXP = tempEXP + mon4.EXP;
            tempGold = tempGold + mon4.GOLD;
            monstersKilled++;
        }
        if (mon5.CurrentHP <= 0)
        {
            mon5.CurrentHP = 999;
            tempEXP = tempEXP + mon5.EXP;
            tempGold = tempGold + mon5.GOLD;
            monstersKilled++;
        }
        if (mon6.CurrentHP <= 0)
        {
            mon6.CurrentHP = 999;
            tempEXP = tempEXP + mon6.EXP;
            tempGold = tempGold + mon6.GOLD;
            monstersKilled++;
        }
        if (mon7.CurrentHP <= 0)
        {
            mon7.CurrentHP = 999;
            tempEXP = tempEXP + mon7.EXP;
            tempGold = tempGold + mon7.GOLD;
            monstersKilled++;
        }
        if (mon8.CurrentHP <= 0)
        {
            mon8.CurrentHP = 999;
            tempEXP = tempEXP + mon8.EXP;
            tempGold = tempGold + mon8.GOLD;
            monstersKilled++;
        }
        if (mon9.CurrentHP <= 0)
        {
            mon9.CurrentHP = 999;
            tempEXP = tempEXP + mon9.EXP;
            tempGold = tempGold + mon9.GOLD;
            monstersKilled++;
        }

        //monChosen value is the amount of player's selection whom to attack. Target# variables represent which monster has chosen. Target# is getting number from the
        //tempTargetedMonster variable
        if (monChosen == 1)
        {
            players[0].GetComponent<DR_PlayerRun>().Target = tempTargetedMonster;
        }
        if (monChosen == 2)
        {
            players[1].GetComponent<DR_PlayerRun>().Target = tempTargetedMonster;
        }
        if (monChosen == 3)
        {
            players[2].GetComponent<DR_PlayerRun>().Target = tempTargetedMonster;
        }
        if (monChosen == 4)
        {
            players[3].GetComponent<DR_PlayerRun>().Target = tempTargetedMonster;
        }

        // if level up happens, call void LevelUP continuously
        if (levelUP)
        {
            LevelUP();
        }

        // if certain(DY_MonsterGroup) amount of monsters are dead, player wins
        if(monstersKilled == monGroup.AmountMons)
        {
            if (!battleWin)
            {
                if (reachPoint < timer)
                {
                    battleWin = true;
                    Victory();
                    monstersKilled = 0;
                    ResetTimer();
                } 
            }
        } // if all players are dead, player lose
        else if (playerStat1.PlayerCurHP <= 0 && playerStat2.PlayerCurHP <= 0 && playerStat3.PlayerCurHP <= 0 && playerStat4.PlayerCurHP <= 0)
        {
            if (!gameOver)
            {
                gameOver = true;
                battleEnd = true;
                Lose();
            }
        }
        else // if win/lose condition is not fulfilled continue the battle calculation
        {
            if (turnScript.EnemyBehaveSwitch)
            {
                if (curTurn < turnOrder.Count)
                {
                    if (reachPoint < timer)
                    {
                        TurnSequence();
                        ResetTimer();
                    }
                }
                else if (curTurn >= turnOrder.Count) // if all characters attacked or defended, battle calculation stops and go back to player's turn order(DY_TurnOrder)
                {
                    if (reachPoint < timer)
                    {
                        turnScript.EnemyBehaveSwitch = false;
                        turnOver = true;
                        TurnOrderRandom();
                        curTurn = 0;
                        goGo = 0;
                        monChosen = 0;
                        attacker.text = "";
                        defender.text = "";
                        damageAmount.text = "";
                        EXPdis.text = "";
                        ResetTimer();
                    }
                }
            }
        }
    }

    void LaneOrderUpdate() // according to lane order number that is attached to CC#, reorder the list of players. this will affect which player is attacking and
    {                      // which player is defending
        players.RemoveRange(0, players.Count);
        for (int i = 0; i < 5; i++)
        {
            if (playerStat1.OrderGrab == i)
            {
                players.Add(GameObject.Find("CC1"));
            }
            else if (playerStat2.OrderGrab == i)
            {
                players.Add(GameObject.Find("CC2"));
            }
            else if (playerStat3.OrderGrab == i)
            {
                players.Add(GameObject.Find("CC3"));
            }
            else if (playerStat4.OrderGrab == i)
            {
                players.Add(GameObject.Find("CC4"));
            }
        }
    }

    void ResetTimer()
    {
        timer = 0;
    }

    void TurnSequence() // if someone is there 
    {
        Targetting();
    }

    void EnemyRun()
    {
        //playerStat1 == leader
        //if(mon1.MOR - 2 * playerStat1.PlayerLevelGrab() + UnityEngine.Random.Range(0, 50) < 80)
    }

    void EnemySpell()
    {

    }

    void EnemySkill()
    {

    }

    void PlayerMagic()
    {

    }

    void Targetting() // targetting whom to attack
    {
        // defending players are being chosen by random number(Player1 = 50%, Player2 = 25%, Player3 = 12.5%, Player4 = 12.5%)
        int randomTargetting = UnityEngine.Random.Range(1, 8);

        //Character 1 Being Targeted
        if (randomTargetting == 1 || randomTargetting == 2 || randomTargetting == 3 || randomTargetting == 4)
        {
            targetedPlayer = 0;
            if(players[targetedPlayer].GetComponent<PlayerStats>().PlayerCurHP <= 0)
            {
                if(players[1].GetComponent<PlayerStats>().PlayerCurHP > 0)
                {
                    targetedPlayer = 1;
                }
                else if (players[2].GetComponent<PlayerStats>().PlayerCurHP > 0)
                {
                    targetedPlayer = 2;
                }
                else if (players[3].GetComponent<PlayerStats>().PlayerCurHP > 0)
                {
                    targetedPlayer = 3;
                }
            }
        }
        //Character 2 Being Targeted
        else if (randomTargetting == 5 || randomTargetting == 6)
        {
            targetedPlayer = 1;
            if (players[targetedPlayer].GetComponent<PlayerStats>().PlayerCurHP <= 0)
            {
                if (players[0].GetComponent<PlayerStats>().PlayerCurHP > 0)
                {
                    targetedPlayer = 0;
                }
                else if (players[2].GetComponent<PlayerStats>().PlayerCurHP > 0)
                {
                    targetedPlayer = 2;
                }
                else if (players[3].GetComponent<PlayerStats>().PlayerCurHP > 0)
                {
                    targetedPlayer = 3;
                }
            }
        }
        //Character 3 Being Targeted
        else if (randomTargetting == 7)
        {
            targetedPlayer = 2;
            if (players[targetedPlayer].GetComponent<PlayerStats>().PlayerCurHP <= 0)
            {
                if (players[0].GetComponent<PlayerStats>().PlayerCurHP > 0)
                {
                    targetedPlayer = 0;
                }
                else if (players[1].GetComponent<PlayerStats>().PlayerCurHP > 0)
                {
                    targetedPlayer = 1;
                }
                else if (players[3].GetComponent<PlayerStats>().PlayerCurHP > 0)
                {
                    targetedPlayer = 3;
                }
            }
        }
        //Character 4 Being Targeted
        else if (randomTargetting == 8)
        {
            targetedPlayer = 3;
            if (players[targetedPlayer].GetComponent<PlayerStats>().PlayerCurHP <= 0)
            {
                if (players[0].GetComponent<PlayerStats>().PlayerCurHP > 0)
                {
                    targetedPlayer = 0;
                }
                else if (players[1].GetComponent<PlayerStats>().PlayerCurHP > 0)
                {
                    targetedPlayer = 1;
                }
                else if (players[2].GetComponent<PlayerStats>().PlayerCurHP > 0)
                {
                    targetedPlayer = 2;
                }
            }
        }

        // monster or player will attack according to order shuffled randomly ex) if curTurn == player, then player will attack
        if (turnOrder[curTurn] == GameObject.Find("Monster1"))
        {
            EnemyHitPercent();
        }
        else if (turnOrder[curTurn] == GameObject.Find("Monster2"))
        {
            EnemyHitPercent();
        }
        else if (turnOrder[curTurn] == GameObject.Find("Monster3"))
        {
            EnemyHitPercent();
        }
        else if (turnOrder[curTurn] == GameObject.Find("Monster4"))
        {
            EnemyHitPercent();
        }
        else if (turnOrder[curTurn] == GameObject.Find("Monster5"))
        {
            EnemyHitPercent();
        }
        else if (turnOrder[curTurn] == GameObject.Find("Monster6"))
        {
            EnemyHitPercent();
        }
        else if (turnOrder[curTurn] == GameObject.Find("Monster7"))
        {
            EnemyHitPercent();
        }
        else if (turnOrder[curTurn] == GameObject.Find("Monster8"))
        {
            EnemyHitPercent();
        }
        else if (turnOrder[curTurn] == GameObject.Find("Monster9"))
        {
            EnemyHitPercent();
        }
        else if(turnOrder[curTurn] == GameObject.Find("CC1"))
        {
            if (turnOrder[curTurn].GetComponent<DR_PlayerRun>().Run)
            {
                RunState();
            }
            else
            {
                PlayerHitPercent();
            }
        }
        else if (turnOrder[curTurn] == GameObject.Find("CC2"))
        {
            if (turnOrder[curTurn].GetComponent<DR_PlayerRun>().Run)
            {
                RunState();
            }
            else
            {
                PlayerHitPercent();
            }
        }
        else if (turnOrder[curTurn] == GameObject.Find("CC3"))
        {
            if (turnOrder[curTurn].GetComponent<DR_PlayerRun>().Run)
            {
                RunState();
            }
            else
            {
                PlayerHitPercent();
            }
        }
        else if (turnOrder[curTurn] == GameObject.Find("CC4"))
        {
            if (turnOrder[curTurn].GetComponent<DR_PlayerRun>().Run)
            {
                RunState();
            }
            else
            {
                PlayerHitPercent();
            }
        }
        if (!battleWin && !gameOver && !runSuccess && !levelUP)
        {
            UI.BattleUI = 2;
        }
    }

    // Calculation for Enemy Hit Percent
    void EnemyHitPercent()
    {
        if (!battleEnd)
        {
            int playerEVA = players[targetedPlayer].GetComponent<PlayerStats>().PlayerAgiGrab();
            int monsterHIT = turnOrder[curTurn].GetComponent<MonsterIdentifier>().ACC;
            int baseChanceToHit = 168;
            int chanceToHit;
            int randForHit;
            playerEVA = playerEVA + 48;
            chanceToHit = baseChanceToHit + monsterHIT;
            if (chanceToHit > 255)
            {
                chanceToHit = 255;
            }
            else
            {
                chanceToHit = chanceToHit - playerEVA;
            }
            randForHit = UnityEngine.Random.Range(0, 200);
            if (chanceToHit >= randForHit)
            {
                EnemyAttack();
            }
            else if (chanceToHit < randForHit)
            {
                attacker.text = "" + turnOrder[curTurn].GetComponent<MonsterIdentifier>().MonsterName;
                defender.text = "" + players[targetedPlayer].GetComponent<PlayerStats>().PlayerNameGrab();
                damageAmount.text = "MISSED";
                Debug.Log(turnOrder[curTurn] + " Missed");
                curTurn++;
            }
            else if (randForHit == 200)
            {
                attacker.text = "" + turnOrder[curTurn].GetComponent<MonsterIdentifier>().MonsterName;
                defender.text = "" + players[targetedPlayer].GetComponent<PlayerStats>().PlayerNameGrab();
                damageAmount.text = "MISSED";
                Debug.Log(turnOrder[curTurn] + " Missed");
                curTurn++;
            }
            else if (randForHit == 0)
            {
                EnemyAttack();
            }
        }
    }

    // Calculation for Enemy Attack
    void EnemyAttack()
    {
        if (!battleEnd)
        {
            int monsterATK = turnOrder[curTurn].GetComponent<MonsterIdentifier>().ATK;
            int playerDEF = players[targetedPlayer].GetComponent<PlayerStats>().PlayerDefGrab();

            if (players[targetedPlayer].GetComponent<PlayerStats>().PlayerCurHP > 0)
            {
                int damage = monsterATK - playerDEF;
                if (damage < 0)
                {
                    damage = 1;
                }
                players[targetedPlayer].GetComponent<PlayerStats>().PlayerCurHP = players[targetedPlayer].GetComponent<PlayerStats>().PlayerCurHP - damage;
                Debug.Log(players[targetedPlayer] + " gets " + damage + " damage from " + turnOrder[curTurn]);
                attacker.text = "" + turnOrder[curTurn].GetComponent<MonsterIdentifier>().MonsterName;
                defender.text = "" + players[targetedPlayer].GetComponent<PlayerStats>().PlayerNameGrab();
                damageAmount.text = damage + " DMG";
            }
            else
            {
                timer = 3;
                Debug.Log("The player has died");
            }
            curTurn++;
            UI.BattleUI = 0;
            ResetTimer();
        }
    }

    // Calculation for Player Hit Percent
    void PlayerHitPercent()
    {
        if (!battleEnd)
        {
            float playerHITvalue = turnOrder[curTurn].GetComponent<PlayerStats>().PlayerHitGrab();
            int monsterEVA = monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().EVA;
            int playerHIT = Convert.ToInt32(playerHITvalue);
            int baseChanceToHit = 168;
            int chanceToHit;
            int randForHit;
            chanceToHit = baseChanceToHit + playerHIT;
            if (chanceToHit > 255)
            {
                chanceToHit = 255;
            }
            else
            {
                chanceToHit = chanceToHit - monsterEVA;
            }
            randForHit = UnityEngine.Random.Range(0, 200);
            if (chanceToHit >= randForHit)
            {
                PlayerAttack();
            }
            else if (chanceToHit < randForHit)
            {
                attacker.text = "" + turnOrder[curTurn].GetComponent<PlayerStats>().PlayerNameGrab();
                defender.text = "" + monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().MonsterName;
                damageAmount.text = "MISSED";
                Debug.Log(turnOrder[curTurn] + " Missed");
                curTurn++;
            }
            else if (randForHit == 200)
            {
                attacker.text = "" + turnOrder[curTurn].GetComponent<PlayerStats>().PlayerNameGrab();
                defender.text = "" + monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().MonsterName;
                damageAmount.text = "MISSED";
                Debug.Log(turnOrder[curTurn] + " Missed");
                curTurn++;
            }
            else if (randForHit == 0)
            {
                PlayerAttack();
            }
        }
    }

    // Calculation for Player Attack
    void PlayerAttack()
    {
        if (!battleEnd)
        {
            if (goGo == 0)
            {
                int playerATK = turnOrder[curTurn].GetComponent<PlayerStats>().PlayerStrengthGrab();
                int monsterDEF = monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().DEF;
                if (monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().Dead == false)
                {
                    int damage = playerATK / 2 - monsterDEF;
                    if (damage < 0)
                    {
                        damage = 1;
                    }
                    //rolls random number for critical hit
                    int crit = UnityEngine.Random.Range(1, 10);
                    //if number is 2 or below add more damage.
                    if (crit <= 2)
                    {
                        damage = 2;
                    }
                    //if number is 3 or higher then keep damage at 1.
                    if (crit >= 3)
                    {
                        damage = 1;
                    }
                    monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().CurrentHP = 
                        monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().CurrentHP - damage;
                    Debug.Log(monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target] + " gets " + damage + " damage from " + turnOrder[curTurn]);
                    attacker.text = "" + turnOrder[curTurn].GetComponent<PlayerStats>().PlayerNameGrab();
                    defender.text = "" + monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().MonsterName;
                    damageAmount.text = damage + " DMG";
                }
                else
                {
                    timer = 3;
                    Debug.Log("Its already dead.");
                }
                goGo = 1; // move onto the next target
                UI.BattleUI = 0;
                ResetTimer();
            }
            else if (goGo == 1)
            {
                int playerATK = turnOrder[curTurn].GetComponent<PlayerStats>().PlayerStrengthGrab();
                int monsterDEF = monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().DEF;

                if (monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().Dead == false)
                {
                    int damage = playerATK / 2 - monsterDEF;
                    if (damage < 0)
                    {
                        damage = 1;
                    }

                    //rolls random number for critical hit
                    int crit = UnityEngine.Random.Range(1, 10);
                    //if number is 2 or below add more damage.
                    if (crit <= 2)
                    {
                        damage = 2;
                    }
                    //if number is 3 or higher then keep damage at 1.
                    if (crit >= 3)
                    {
                        damage = 1;
                    }
                    monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().CurrentHP = 
                        monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().CurrentHP - damage;
                    Debug.Log(monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target] + " gets " + damage + " damage from " + turnOrder[curTurn]);
                    attacker.text = "" + turnOrder[curTurn].GetComponent<PlayerStats>().PlayerNameGrab();
                    defender.text = "" + monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().MonsterName;
                    damageAmount.text = damage + " DMG";
                }
                else
                {
                    timer = 3;
                    Debug.Log("Its already dead.");
                }
                goGo = 2;
                UI.BattleUI = 0;
                ResetTimer();
            }
            else if (goGo == 2)
            {
                int playerATK = turnOrder[curTurn].GetComponent<PlayerStats>().PlayerStrengthGrab();
                int monsterDEF = monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().DEF;

                if (monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().Dead == false)
                {
                    int damage = playerATK / 2 - monsterDEF;
                    if (damage < 0)
                    {
                        damage = 1;
                    }

                    //rolls random number for critical hit
                    int crit = UnityEngine.Random.Range(1, 10);
                    //if number is 2 or below add more damage.
                    if (crit <= 2)
                    {
                        damage = 2;
                    }
                    //if number is 3 or higher then keep damage at 1.
                    if (crit >= 3)
                    {
                        damage = 1;
                    }
                    monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().CurrentHP = 
                        monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().CurrentHP - damage;
                    Debug.Log(monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target] + " gets " + damage + " damage from " + turnOrder[curTurn]);
                    attacker.text = "" + turnOrder[curTurn].GetComponent<PlayerStats>().PlayerNameGrab();
                    defender.text = "" + monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().MonsterName;
                    damageAmount.text = damage + " DMG";
                }
                else
                {
                    timer = 3;
                    Debug.Log("Its already dead.");
                }
                goGo = 3;
                UI.BattleUI = 0;
                ResetTimer();
            }
            else if (goGo == 3)
            {
                int playerATK = turnOrder[curTurn].GetComponent<PlayerStats>().PlayerStrengthGrab();
                int monsterDEF = monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().DEF;

                if (monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().Dead == false)
                {
                    int damage = playerATK / 2 - monsterDEF;
                    if (damage < 0)
                    {
                        damage = 1;
                    }

                    //rolls random number for critical hit
                    int crit = UnityEngine.Random.Range(1, 10);
                    //if number is 2 or below add more damage.
                    if (crit <= 2)
                    {
                        damage = 2;
                    }
                    //if number is 3 or higher then keep damage at 1.
                    if (crit >= 3)
                    {
                        damage = 1;
                    }
                    monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().CurrentHP = 
                        monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().CurrentHP - damage;
                    Debug.Log(monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target] + " gets " + damage + " damage from " + turnOrder[curTurn]);
                    attacker.text = "" + turnOrder[curTurn].GetComponent<PlayerStats>().PlayerNameGrab();
                    defender.text = "" + monsters[turnOrder[curTurn].GetComponent<DR_PlayerRun>().Target].GetComponent<MonsterIdentifier>().MonsterName;
                    damageAmount.text = damage + " DMG";
                }
                else
                {
                    timer = 3;
                    Debug.Log("Its already dead.");
                }
                goGo = 0;
            }
            UI.BattleUI = 0;
            curTurn++;
        }
    }

    void TurnOrderRandom() // Turn order is randomly shuffled
    {
        for (int i = 0; i < turnOrder.Count; i++)
        {
            int rnd = UnityEngine.Random.Range(0, turnOrder.Count);
            tempGO = turnOrder[rnd];
            turnOrder[rnd] = turnOrder[i];
            turnOrder[i] = tempGO;
        }
    }

    // Rewards
    void Victory()
    {
        UI.BattleUI = 3;
        attacker.text = "EXP";
        defender.text = "GOLD";
        EXPdis.text = "" + tempEXP;
        damageAmount.text = "" + tempGold;
        playerStat1.PlayerCurExpGrab = playerStat1.PlayerCurExpGrab + tempEXP;
        playerStat2.PlayerCurExpGrab = playerStat2.PlayerCurExpGrab + tempEXP;
        playerStat3.PlayerCurExpGrab = playerStat3.PlayerCurExpGrab + tempEXP;
        playerStat4.PlayerCurExpGrab = playerStat4.PlayerCurExpGrab + tempEXP;
        //inventory.pCurrency = inventory.pCurrency + tempGold;
        if (reachPoint < timer)
        {
            // if player level up
            if (player1maxEXP < playerStat1.PlayerCurExpGrab || player2maxEXP < playerStat1.PlayerCurExpGrab || player3maxEXP < playerStat1.PlayerCurExpGrab
                || player4maxEXP < playerStat1.PlayerCurExpGrab)
            {
                levelUP = true;
            }
            else
            {
                battleEnd = true;
            }
            ResetTimer();
        }
        tempEXP = 0;
    }

    void LevelUP() // after winning
    {
        if(reachPoint < timer)
        {
            UI.BattleUI = 6;
            attacker.text = "Lev. UP";
            defender.text = "HP max";
            if (player1maxEXP <= playerStat1.PlayerCurExpGrab && i == 0)
            {
                EXPdis.text = "" + players[0].GetComponent<PlayerStats>().PlayerNameGrab() + "  " + players[0].GetComponent<PlayerStats>().PlayerLevelGrab();
                damageAmount.text = "" + players[0].GetComponent<PlayerStats>().PlayerMaxHPGrab() + "pts.";
                loseText.text = "Str UP. Agi UP. Luck UP. Vit UP.";
                i++;
                ResetTimer();
            }
            else if (player2maxEXP <= playerStat1.PlayerCurExpGrab && i == 1)
            {
                EXPdis.text = "" + players[1].GetComponent<PlayerStats>().PlayerNameGrab() + "  " + players[1].GetComponent<PlayerStats>().PlayerLevelGrab();
                damageAmount.text = "" + players[1].GetComponent<PlayerStats>().PlayerMaxHPGrab() + "pts.";
                loseText.text = "Str UP. Agi UP. Luck UP. Vit UP.";
                i++;
                ResetTimer();
            }
            else if (player3maxEXP <= playerStat1.PlayerCurExpGrab && i == 2)
            {
                EXPdis.text = "" + players[2].GetComponent<PlayerStats>().PlayerNameGrab() + "  " + players[2].GetComponent<PlayerStats>().PlayerLevelGrab();
                damageAmount.text = "" + players[2].GetComponent<PlayerStats>().PlayerMaxHPGrab() + "pts.";
                loseText.text = "Str UP. Agi UP. Luck UP. Vit UP.";
                i++;
                ResetTimer();
            }
            else if (player4maxEXP <= playerStat1.PlayerCurExpGrab && i == 3)
            {
                EXPdis.text = "" + players[3].GetComponent<PlayerStats>().PlayerNameGrab() + "  " + players[3].GetComponent<PlayerStats>().PlayerLevelGrab();
                damageAmount.text = "" + players[3].GetComponent<PlayerStats>().PlayerMaxHPGrab() + "pts.";
                loseText.text = "Str UP. Agi UP. Luck UP. Vit UP.";
                i++;
                ResetTimer();
            }
            else if (i >= 4)
            {
                attacker.text = "";
                defender.text = "";
                EXPdis.text = "";
                damageAmount.text = "";
                loseText.text = "";
                i = 0;
                levelUP = false;
                battleEnd = true;
                ResetTimer();
            }
        }
    }

    void Lose()
    {
        attacker.text = "";
        defender.text = "";
        damageAmount.text = "";
        EXPdis.text = "";
        UI.BattleUI = 4;
        loseText.text = "Your Party Terminated";
    }

    // Calculation for Player Run
    public void RunState()
    {
        //gets random number between 1 and 10
        int runRandom = UnityEngine.Random.Range(1, 10);
        //if number is 7 or higher then the run is successful
        if (runRandom >= 7)
        {       
                attacker.text = "";
                defender.text = "";
                damageAmount.text = "";
                EXPdis.text = "";
                loseText.text = "Successful Run!";
                runSuccess = true;
                battleEnd = true;
            
        }
        //if the number is 6 or lower the run is a failure
        if (runRandom <= 6)
        {
            Debug.Log("Run Fail!");
            turnOrder[curTurn].GetComponent<DR_PlayerRun>().Run = false;
            ResetTimer();
            curTurn++;
        }
        
    }

    public int TargettedMonster { get { return tempTargetedMonster; } set { tempTargetedMonster = value; } }
    public int MonChosen { get { return monChosen; } set { monChosen = value; } }
    public bool TurnOver { get { return turnOver; } set { turnOver = value; } }
    public bool RunSuccess { get { return runSuccess; } set { runSuccess = value; } }
    public bool GameOver { get { return gameOver; } set { gameOver = value; } }
    public bool BattleWin { get { return battleWin; } set { battleWin = value; } }
    public bool LevelUp { get { return levelUP; } set { levelUP = value; } }
    public bool BattleEnd { get { return battleEnd; } set { battleEnd = value; } }
}
