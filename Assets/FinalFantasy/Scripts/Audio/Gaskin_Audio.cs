﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gaskin_Audio : MonoBehaviour
    /* Code By: Delroy Gaskin
     * Purpose: This script causes the audio sound to play when the player collides with with.
     */ 
{
    public AudioSource Cancel;

    private void Start()
    {
        AudioSource Cancel = GetComponent<AudioSource>();

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Contains("Player"))
        {
            Cancel.Play();
        }
    }

}
