﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*-------------------------------------------------------------------------------------------------------------------------------
* Name: Dongwon Yoo
 * Date: 1/1/16
 * Credit: Final Fantasy 1
 * Purpose: This Script is Changing the name and hp indicators in the scene.
 --------------------------------------------------------------------------------------------------------------------------*/

public class DY_HPIndicator : MonoBehaviour
{

    PlayerStats character1;
    PlayerStats character2;
    PlayerStats character3;
    PlayerStats character4;

    TextMesh HPindicator1;                  // indicator in the right side(Name and Current HP)
    TextMesh HPindicator2;                  // indicator in the right side(Name and Current HP)
    TextMesh HPindicator3;                  // indicator in the right side(Name and Current HP)
    TextMesh HPindicator4;                  // indicator in the right side(Name and Current HP)
                                            // indicator in the right side(Name and Current HP)
    TextMesh NameIndicator1;                // indicator in the right side(Name and Current HP)
    TextMesh NameIndicator2;                // indicator in the right side(Name and Current HP)
    TextMesh NameIndicator3;                // indicator in the right side(Name and Current HP)
    TextMesh NameIndicator4;                // indicator in the right side(Name and Current HP)

    [SerializeField] List<GameObject> players = new List<GameObject>(); // amount of players


    // Use this for initialization
    void Start ()
    {
        character1 = GameObject.Find("CC1").GetComponent<PlayerStats>();
        character2 = GameObject.Find("CC2").GetComponent<PlayerStats>();
        character3 = GameObject.Find("CC3").GetComponent<PlayerStats>();
        character4 = GameObject.Find("CC4").GetComponent<PlayerStats>();

        HPindicator1 = GameObject.Find("HPIndicator1").GetComponent<TextMesh>();
        HPindicator2 = GameObject.Find("HPIndicator2").GetComponent<TextMesh>();
        HPindicator3 = GameObject.Find("HPIndicator3").GetComponent<TextMesh>();
        HPindicator4 = GameObject.Find("HPIndicator4").GetComponent<TextMesh>();

        NameIndicator1 = GameObject.Find("P1Name").GetComponent<TextMesh>();
        NameIndicator2 = GameObject.Find("P2Name").GetComponent<TextMesh>();
        NameIndicator3 = GameObject.Find("P3Name").GetComponent<TextMesh>();
        NameIndicator4 = GameObject.Find("P4Name").GetComponent<TextMesh>();

        LaneOrderUpdate();
    }

    private void OnDisable()
    {
        players.RemoveRange(0, players.Count);
    }

    private void OnEnable()
    {
        Start();
    }

    // Update is called once per frame
    void Update ()
    {
        if (character1.PlayerCurHP > 0) // if player is survived, display current HP
        {
            HPindicator1.text = "" + players[0].GetComponent<PlayerStats>().PlayerCurHP;
        }
        if (character2.PlayerCurHP > 0)
        {
            HPindicator2.text = "" + players[1].GetComponent<PlayerStats>().PlayerCurHP;
        }
        if (character3.PlayerCurHP > 0)
        {
            HPindicator3.text = "" + players[2].GetComponent<PlayerStats>().PlayerCurHP;
        }
        if (character4.PlayerCurHP > 0)
        {
            HPindicator4.text = "" + players[3].GetComponent<PlayerStats>().PlayerCurHP;
        }
        // names will be displayed even if the player is dead
        NameIndicator1.text = "" + players[0].GetComponent<PlayerStats>().PlayerNameGrab();
        NameIndicator2.text = "" + players[1].GetComponent<PlayerStats>().PlayerNameGrab();
        NameIndicator3.text = "" + players[2].GetComponent<PlayerStats>().PlayerNameGrab();
        NameIndicator4.text = "" + players[3].GetComponent<PlayerStats>().PlayerNameGrab();


        // this will show if player is dead
        if (players[0].GetComponent<PlayerStats>().PlayerCurHP <= 0)
        {
            HPindicator1.text = "Dead";
        }
        if (players[1].GetComponent<PlayerStats>().PlayerCurHP <= 0)
        {
            HPindicator2.text = "Dead";
        }
        if (players[2].GetComponent<PlayerStats>().PlayerCurHP <= 0)
        {
            HPindicator3.text = "Dead";
        }
        if (players[3].GetComponent<PlayerStats>().PlayerCurHP <= 0)
        {
            HPindicator4.text = "Dead";
        }
    }

    // according to lane order number that is attached to CC#, reorder the list of players. this will affect the order of indicators above
    void LaneOrderUpdate()
    {
        players.RemoveRange(0, players.Count);
        if (character1.OrderGrab == 1)
        {
            players.Add(GameObject.Find("CC1"));
        }
        else if (character2.OrderGrab == 1)
        {
            players.Add(GameObject.Find("CC2"));
        }
        else if (character3.OrderGrab == 1)
        {
            players.Add(GameObject.Find("CC3"));
        }
        else if (character4.OrderGrab == 1)
        {
            players.Add(GameObject.Find("CC4"));
        }

        if (character1.OrderGrab == 2)
        {
            players.Add(GameObject.Find("CC1"));
        }
        else if (character2.OrderGrab == 2)
        {
            players.Add(GameObject.Find("CC2"));
        }
        else if (character3.OrderGrab == 2)
        {
            players.Add(GameObject.Find("CC3"));
        }
        else if (character4.OrderGrab == 2)
        {
            players.Add(GameObject.Find("CC4"));
        }

        if (character1.OrderGrab == 3)
        {
            players.Add(GameObject.Find("CC1"));
        }
        else if (character2.OrderGrab == 3)
        {
            players.Add(GameObject.Find("CC2"));
        }
        else if (character3.OrderGrab == 3)
        {
            players.Add(GameObject.Find("CC3"));
        }
        else if (character4.OrderGrab == 3)
        {
            players.Add(GameObject.Find("CC4"));
        }

        if (character1.OrderGrab == 4)
        {
            players.Add(GameObject.Find("CC1"));
        }
        else if (character2.OrderGrab == 4)
        {
            players.Add(GameObject.Find("CC2"));
        }
        else if (character3.OrderGrab == 4)
        {
            players.Add(GameObject.Find("CC3"));
        }
        else if (character4.OrderGrab == 4)
        {
            players.Add(GameObject.Find("CC4"));
        }
    }
}
