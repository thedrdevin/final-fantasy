﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DY_Controller : MonoBehaviour
{
    Vector3 pos;
    Animator anim;
    [SerializeField] float speed = 2.0f;
    [SerializeField] float timerForMovement;
    [SerializeField] int movement;
    [SerializeField] int rand;
    [SerializeField] int randforMon;
    [SerializeField] GameObject o_monGroup;
    DY_EnemyEncounter enemyEncounter;
    DY_MonsterGroup monGroup;

	// Use this for initialization
	void Start ()
    {
        pos = transform.position;
        anim = GetComponent<Animator>();
        enemyEncounter = GameObject.Find("EnemyEncounter").GetComponent<DY_EnemyEncounter>();
        monGroup = o_monGroup.GetComponent<DY_MonsterGroup>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKey(KeyCode.LeftArrow) && transform.position == pos)
        {
            pos += Vector3.left;
            anim.SetInteger("Move", 1);
            timerForMovement += Time.deltaTime;
            rand = UnityEngine.Random.Range(0, 5);
            randforMon = UnityEngine.Random.Range(0, 5);
            monGroup.MonsterGroup = randforMon;
            monGroup.Fight = false;
        }
        else if (Input.GetKey(KeyCode.RightArrow) && transform.position == pos)
        {
            pos += Vector3.right;
            anim.SetInteger("Move", 3);
            timerForMovement += Time.deltaTime;
            rand = UnityEngine.Random.Range(0, 5);
            randforMon = UnityEngine.Random.Range(0, 5);
            monGroup.MonsterGroup = randforMon;
            monGroup.Fight = false;
        }
        else if (Input.GetKey(KeyCode.UpArrow) && transform.position == pos)
        {
            pos += Vector3.up;
            anim.SetInteger("Move", 4);
            timerForMovement += Time.deltaTime;
            rand = UnityEngine.Random.Range(0, 5);
            randforMon = UnityEngine.Random.Range(0, 5);
            monGroup.MonsterGroup = randforMon;
            monGroup.Fight = false;
        }
        else if (Input.GetKey(KeyCode.DownArrow) && transform.position == pos)
        {
            pos += Vector3.down;
            anim.SetInteger("Move", 2);
            timerForMovement += Time.deltaTime;
            rand = UnityEngine.Random.Range(0, 5);
            randforMon = UnityEngine.Random.Range(0, 5);
            monGroup.MonsterGroup = randforMon;
            monGroup.Fight = false;
        }
        transform.position = Vector3.MoveTowards(transform.position, pos, Time.deltaTime * speed);

        if(timerForMovement > 0.01f)
        {
            timerForMovement = 0;
            movement++;
        }

        if(movement >= 1 && movement <= 10)
        {
            if(rand >= 5)
            {
                enemyEncounter.EnemyEncounter();
                movement = 0;
                rand = 0;
            }
        }
        else if(movement > 10 && movement <= 20)
        {
            if(rand >= 4)
            {
                enemyEncounter.EnemyEncounter();
                movement = 0;
                rand = 0;
            }
        }
        else if (movement > 20 && movement <= 30)
        {
            if (rand >= 3)
            {
                enemyEncounter.EnemyEncounter();
                movement = 0;
                rand = 0;
            }
        }
        else if (movement > 30 && movement <= 40)
        {
            if (rand >= 2)
            {
                enemyEncounter.EnemyEncounter();
                movement = 0;
                rand = 0;
            }
        }
        else if (movement > 40 && movement <= 50)
        {
            if (rand >= 1)
            {
                enemyEncounter.EnemyEncounter();
                movement = 0;
                rand = 0;
            }
        }
        else if(movement > 50)
        {
            movement = 0;
        }
    }
}
