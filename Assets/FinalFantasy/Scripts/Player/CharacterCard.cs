﻿/*-------------------------------------------------------------------------------------------------------------------------------
* Name: Kyle Harrington
 * Date: 1/17/2018
 * Credit: Project : FinalFantasy 1 Nes Remake
 * Purpose: This script is is used to pull the values of characters for the pause menu in which they are displayed in the right hand side of the screen
 * 
 --------------------------------------------------------------------------------------------------------------------------*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class CharacterCard : MonoBehaviour
{ //This script calls all of the characters values and sets them inside of the pause menu (visible via charactercards)
    [SerializeField]
    private GameObject ThisCharacter;
    private PlayerStats playerstats;

    //declaring textmeshes to be called and set
    [SerializeField]
    private TextMesh nameText;
    [SerializeField]
    private TextMesh CurHpText;
    [SerializeField]
    private TextMesh LevelText;

    //declaring private variables that wont change after opening the pause menu
    private int CurHpValue;
    private int CurLevelValue;
    private int MaxHpValue;

    private void Start()
    {
        
    }
    // Use this for initialization
    void Update () //detects the tag of each CC object and stores their values inside of textmeshes in the pause menu
    {
        if (ThisCharacter.tag == "CC1")
        {
            playerstats = GameObject.FindGameObjectWithTag("CC1").GetComponent<PlayerStats>().myPlayerStats;
            nameText = GetComponentInChildren<TextMesh>();
            nameText.text = "" + playerstats.PlayerNameGrab();
            CurHpValue = playerstats.PlayerCurHP;
            MaxHpValue = playerstats.PlayerMaxHPGrab();
            CurHpText.text = "HP  " + CurHpValue + "/" + MaxHpValue.ToString();
            CurLevelValue = playerstats.PlayerLevelGrab();
            LevelText.text = "L " + CurLevelValue.ToString();
        }
        else if (ThisCharacter.tag == "CC2")
        {
            playerstats = GameObject.FindGameObjectWithTag("CC2").GetComponent<PlayerStats>().myPlayerStats;
            nameText = GetComponentInChildren<TextMesh>();
            nameText.text = "" + playerstats.PlayerNameGrab();
            CurHpValue = playerstats.PlayerCurHP;
            MaxHpValue = playerstats.PlayerMaxHPGrab();
            CurHpText.text = "HP  " + CurHpValue + "/" + MaxHpValue.ToString();
            CurLevelValue = playerstats.PlayerLevelGrab();
            LevelText.text = "L " + CurLevelValue.ToString();
        }
        else if (ThisCharacter.tag == "CC3")
        {
            playerstats = GameObject.FindGameObjectWithTag("CC3").GetComponent<PlayerStats>().myPlayerStats;
            nameText = GetComponentInChildren<TextMesh>();
            nameText.text = "" + playerstats.PlayerNameGrab();
            CurHpValue = playerstats.PlayerCurHP;
            MaxHpValue = playerstats.PlayerMaxHPGrab();
            CurHpText.text = "HP  " + CurHpValue + "/" + MaxHpValue.ToString();
            CurLevelValue = playerstats.PlayerLevelGrab();
            LevelText.text = "L " + CurLevelValue.ToString();
        }
        else if (ThisCharacter.tag == "CC4")
        {
            playerstats = GameObject.FindGameObjectWithTag("CC4").GetComponent<PlayerStats>().myPlayerStats;
            nameText = GetComponentInChildren<TextMesh>();
            nameText.text = "" + playerstats.PlayerNameGrab();
            CurHpValue = playerstats.PlayerCurHP;
            MaxHpValue = playerstats.PlayerMaxHPGrab();
            CurHpText.text = "HP  " + CurHpValue + "/" + MaxHpValue.ToString();
            CurLevelValue = playerstats.PlayerLevelGrab();
            LevelText.text = "L " + CurLevelValue.ToString();
        }
    }
}
