﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DY_StatControl : MonoBehaviour
{
    PlayerStats playerStat1;
    PlayerStats playerStat2;
    PlayerStats playerStat3;
    PlayerStats playerStat4;

    [SerializeField] private int p1HP;
    [SerializeField] private int p2HP;
    [SerializeField] private int p3HP;
    [SerializeField] private int p4HP;

    private bool a;

    // Use this for initialization
    void Start ()
    {
        playerStat1 = GameObject.Find("CC1").GetComponent<PlayerStats>();
        playerStat2 = GameObject.Find("CC2").GetComponent<PlayerStats>();
        playerStat3 = GameObject.Find("CC3").GetComponent<PlayerStats>();
        playerStat4 = GameObject.Find("CC4").GetComponent<PlayerStats>();

    }
	
	// Update is called once per frame
	void Update ()
    {
        p1HP = playerStat1.PlayerCurHP;
        p2HP = playerStat2.PlayerCurHP;
        p3HP = playerStat3.PlayerCurHP;
        p4HP = playerStat4.PlayerCurHP;

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            playerStat1.PlayerCurHP = playerStat1.PlayerCurHP - 10;
            playerStat2.PlayerCurHP = playerStat2.PlayerCurHP - 10;
            playerStat3.PlayerCurHP = playerStat3.PlayerCurHP - 10;
            playerStat4.PlayerCurHP = playerStat4.PlayerCurHP - 10;
        }
    }
}
