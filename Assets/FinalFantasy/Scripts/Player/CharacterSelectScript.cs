﻿/*-------------------------------------------------------------------------------------------------------------------------------
* Name: Kyle Harrington
 * Date: 1/17/2018
 * Credit: Project : FinalFantasy 1 Nes Remake
 * Purpose: This script allows the status page object to select the correct character values when the character is selected 
 --------------------------------------------------------------------------------------------------------------------------*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class CharacterSelectScript : MonoBehaviour {
    //used to set playerchosen value for use in the status page script
    [SerializeField]
    private int _PlayerChosen;

    public int PlayerChosen
    {
        get { return _PlayerChosen; }
        set { _PlayerChosen = value; }
    }
}
