﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SScript : MonoBehaviour {

    //Code By: Rahul Yerramneedi
    //Feature: This is the mian gil script, it basically is like a score script and will hold the main value of the script.

    public static int scorevalue = 0;
    Text score;

	// Use this for initialization
	void Start () {
		score = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        score.text = "Gil: " + scorevalue;
	}
}
