﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    GameObject inventoryPanel;
    [SerializeField]
    GameObject slotPanel;
    public GameObject inventorySlot;
    public GameObject inventoryItem;
    ItemsDataBase iDatabase;
    private ItemData data;

    public int pCurrency { get; set; }
    public static Inventory instance;

    // amount of slots that will be available in the players' inventory
    private int numSlots;



    // Back-end representation of inventory
    public List<Objects> gobjects = new List<Objects>();

    public List<GameObject> slots = new List<GameObject>();


    // real time inventory


    private void Awake()
    {
        instance = this;
        pCurrency = 200;

    }




    // Use this for initialization
    void Start()
    {
        iDatabase = GetComponent<ItemsDataBase>();

        inventoryPanel = GameObject.Find("Inventory Panel");
        //slotPanel = inventoryPanel.transform.Find("Slot Panel").gameObject;
        numSlots = 20;
        for (int i = 0; i < numSlots; i++)
        {
            gobjects.Add(new Objects());
            slots.Add(Instantiate(inventorySlot));
            slots[i].transform.SetParent(slotPanel.transform);
        }

        
        
        AddObjectItem(0);
        AddObjectItem(1);
        AddObjectItem(2);
        AddObjectItem(3);
        AddObjectItem(4);
        AddObjectItem(5);
        AddObjectItem(6);
        AddObjectItem(7);
        AddObjectItem(8);
        
    }


    public void AddObjectItem(int id)
    {
        Objects objectToAdd = iDatabase.FetchObjectByID(id);
        if (objectToAdd.Stackable && CheckInventoryForObject(objectToAdd))
        {
            for (int i = 0; i < gobjects.Count; i++)
            {
                if (gobjects[i].ID == id)
                {
                    data = slots[i].transform.GetComponent<ItemData>();
                    data.amount++;
                    data.transform.GetChild(0).GetComponent<Text>().text = data.amount.ToString();
                    Debug.Log(data.amount);
                    break;
                }
            }
        }

       for (int i = 0; i < gobjects.Count; i++)
        {
            if (gobjects[i].ID == -1)
            {
                gobjects[i] = objectToAdd;
                GameObject gameObj = Instantiate(inventoryItem);

                gameObj.transform.SetParent(slots[i].transform);
                gameObj.transform.position = Vector2.zero;
                gameObj.GetComponent<Image>().sprite = objectToAdd.Sprite;
                gameObj.GetComponentInChildren<Text>().text = objectToAdd.Title;
                break;
            }
        }  
    }
    public void RemoveObjectItem(int id)
    {
        for (int i = 0; i < gobjects.Count; i++)
        {
            if (gobjects[i].ID != -1)
            {
                new Object();
                break;
            }
        }
    }
    bool CheckInventoryForObject(Objects objects)
    {
        for (int i = 0; i < gobjects.Count; i++)
        {
            if (gobjects[i].ID == objects.ID)
            {
               
                return true;
                
            }
        }
        return false;
        
    }
    public void Remove(Objects thisobject)
    {
        gobjects.Remove(thisobject);
    }

    // Update is called once per frame
    void Update()
    {


        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            AddObjectItem(0);

        }
    }

    public int GetCurrency()
    {
        return pCurrency; 
    }

    public int SetCurrency(int gil)
    {
        pCurrency = gil;
        return pCurrency;
    }
}