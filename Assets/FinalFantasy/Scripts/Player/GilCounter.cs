﻿/*-------------------------------------------------------------------------------------------------------------------------------
* Name: Kyle Harrington
 * Date: 1/17/2018
 * Credit: Project : FinalFantasy 1 Nes Remake
 * Purpose: This script calls and returns the value of the Gil in the players inventory and then sets the text mesh to display the given value
 * 
 --------------------------------------------------------------------------------------------------------------------------*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GilCounter : MonoBehaviour {
    private int GilCount;
    [SerializeField]
    private TextMesh GilText;
    [SerializeField]
    private Inventory InventoryScript; 

	// Use this for initialization
	void Start ()
    {

	}
	
	// Update is called once per frame
	void Update ()
    {
        InventoryScript = GameObject.Find("Inventory").GetComponent<Inventory>();
        GilText = GameObject.Find("GilCounter").GetComponentInChildren<TextMesh>();
        GilText.text = Inventory.instance.pCurrency.ToString() + "     GIL";
    }
}
