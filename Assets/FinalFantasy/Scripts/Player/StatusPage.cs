﻿/*-------------------------------------------------------------------------------------------------------------------------------
* Name: Kyle Harrington
 * Date: 1/17/2018
 * Credit: Project : FinalFantasy 1 Nes Remake
 * Purpose: This script finds the selected character's stats for use in the statusPage and then sets the prefab text meshes to display them accurately 
 * 
 --------------------------------------------------------------------------------------------------------------------------*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;


public class StatusPage : MonoBehaviour
{
    [SerializeField]
    private GameObject ThisCharacter;
    private GameObject fortheName;
    private PlayerStats playerstats;

    //declaring textmeshes to be set further in the script
    private TextMesh nameText;
    private TextMesh LevelText;
    private TextMesh StrText;
    private TextMesh AgiText;
    private TextMesh IntText;
    private TextMesh VitText;
    private TextMesh LuckText;
    private TextMesh DmgText;
    private TextMesh HitText;
    private TextMesh AbsorbText;
    private TextMesh EvaText;
    private TextMesh CurExpText;
    private TextMesh NeedExpText;
    private int CurLevelValue;
    [SerializeField]
    private GameObject SP_toMenu; //declared in order to turn the status screen back to the pause menu during update
    PM_CursorNav CurrentState;
    PM_CursorNav ChosenNumber; //used to determine which character card is set in the status page for display

    // Use this for initialization
    void Start () //finds cursor2 object and sets the variable "ThisCharacter" as that object and then sets chosen number equal to the value found within the PM_CursorNav Script
    {
        //set state of state machine to item
        
        ThisCharacter = GameObject.Find("Cursor2");
        ChosenNumber = ThisCharacter.GetComponent<PM_CursorNav>();
    }
	
	// Update is called once per frame
	void Update () //turns the status page display off to reveal the pause menu
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SP_toMenu.SetActive(false);
            //SwitchInMenu();
        }
        //checks chosen number and sets the values shown in status page based on its value
        if (ChosenNumber.PlayerChosen == 1)
        {
            fortheName = GameObject.Find("CC1");
            SetUpText();
        }
        else if (ChosenNumber.PlayerChosen == 2)
        {
            fortheName = GameObject.Find("CC2");
            SetUpText();
        }

        else if (ChosenNumber.PlayerChosen == 3)
        {
            fortheName = GameObject.Find("CC3");
            SetUpText();
        }

        else if (ChosenNumber.PlayerChosen == 4)
        {
            fortheName = GameObject.Find("CC4");
            SetUpText();
        }
    }

void SetUpText() //function that gets all textmeshes inside the status page and sets them according to the selected character card
    {
        playerstats = fortheName.GetComponent<PlayerStats>();
        nameText = GameObject.Find("statusNameText").GetComponent<TextMesh>();
        LevelText = GameObject.Find("CharacterLevelText").GetComponent<TextMesh>();
        StrText = GameObject.Find("StrengthText").GetComponent<TextMesh>();
        AgiText = GameObject.Find("AgilityText").GetComponent<TextMesh>();
        IntText = GameObject.Find("IntelText").GetComponent<TextMesh>();
        VitText = GameObject.Find("VitalText").GetComponent<TextMesh>();
        LuckText = GameObject.Find("LckText").GetComponent<TextMesh>();
        DmgText = GameObject.Find("DamageText").GetComponent<TextMesh>();
        HitText = GameObject.Find("Hit%Text").GetComponent<TextMesh>();
        AbsorbText = GameObject.Find("AbsText").GetComponent<TextMesh>();
        EvaText = GameObject.Find("EvadeText").GetComponent<TextMesh>();
        CurExpText = GameObject.Find("CurrentExpText").GetComponent<TextMesh>();
        NeedExpText = GameObject.Find("NeededExpText").GetComponent<TextMesh>();
        nameText.text = "" + playerstats.PlayerNameGrab();
        LevelText.text = "LEV " + playerstats.PlayerLevelGrab();
        StrText.text = "STR. " + playerstats.PlayerStrengthGrab();
        AgiText.text = "AGL. " + playerstats.PlayerAgiGrab();
        IntText.text = "INT. " + playerstats.PlayerIntGrab();
        VitText.text = "VIT. " + playerstats.PlayerVitGrab();
        LuckText.text = "LUCK " + playerstats.PlayerLuckGrab();
        DmgText.text = "DAMAGE " + playerstats.PlayerAtkGrab();
        HitText.text = "HIT % " + playerstats.PlayerHitGrab();
        AbsorbText.text = "ABSORB " + playerstats.PlayerDefGrab();
        EvaText.text = "EVADE % " + playerstats.PlayerEvaGrab();
        CurExpText.text = "EXP. POINTS  " + playerstats._CurExpGrab();
        NeedExpText.text = "FOR LEV UP  " + playerstats._ExpToLevel();
    }
    
}
