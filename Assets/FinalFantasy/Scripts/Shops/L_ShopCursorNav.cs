﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

/*Code By Mike Looney 
 Code is for navigating Specifically the Item Shop, but can be used as a template for other shops*/

public class L_ShopCursorNav : MonoBehaviour
{
    private L_ShopCursor cursor; //reference to PM_Cursor script to get tranforms for the cursor and access to the reset timer script
    private StatusPage select; //reference to the status page script, used to set value for the character chosen to be displayed in the status screen
    private CharacterSelectScript Selection; //reference to the character select script that determines the character selected

    private int playerChosen;
    [SerializeField]
    private GameObject Status; //holds the gameobject "status block template" for reference, sets to active to display appropriate character stats in the pause menu
    [SerializeField]

    private GameObject ItemSelect; //Text boxes within the shop scene
    public GameObject MenuInteractable;
    public GameObject ShopkeeperTextBox;
    public GameObject ShopkeeperTextBox2;
    public GameObject ShopkeeperTextBox3;
    public GameObject ShopkeeperTextBox4;
    public GameObject ShopkeeperTextBox5;
    public GameObject BuyYN;
    public bool HealObtained;

    enum STATES //states for the state machine 
    {
        Buy,
        Exit,
        Heal,
        Pure,
        Tent,
        BuyYes,
        BuyNo

    }
    STATES curState = STATES.Buy; //sets current state on load
    Dictionary<STATES, Action> stateRegistry = new Dictionary<STATES, Action>(); //allows states to be set inside the dictionary

    // Use this for initialization

    void Start()
    {
        //Status = GameObject.Find("StatusBlockTemplate");
        cursor = GameObject.Find("Cursor2").GetComponent<L_ShopCursor>(); //finds the game object cursor 2 and sets it as "cursor" for local reference

        stateRegistry.Add(STATES.Buy, new Action(BuyState));
        stateRegistry.Add(STATES.Exit, new Action(ExitState));
        stateRegistry.Add(STATES.Heal, new Action(HealState));
        stateRegistry.Add(STATES.Pure, new Action(PureState));
        stateRegistry.Add(STATES.Tent, new Action(TentState));
        stateRegistry.Add(STATES.BuyYes, new Action(BuyYesState));
        stateRegistry.Add(STATES.BuyNo, new Action(BuyNoState));
    }

    // Update is called once per frame
    void Update()
    {
        //allows the states to exist during play and activates functions within that state
        stateRegistry[curState].Invoke();

        if (curState == STATES.Buy)
        {
            BuyState();
        }

        else if (curState == STATES.Exit)
        {
            ExitState();
        }
        else if (curState == STATES.Heal)
        {
            HealState();
        }
        else if (curState == STATES.Pure)
        {
            PureState();
        }
        else if (curState == STATES.Tent)
        {
            TentState();
        }
    }

    void SetState(STATES newState) //sets states 
    {
        curState = newState;
    }

    void BuyState()
    {
        if (cursor.TimerCap <= cursor.TimerTracked)
        {
            cursor.BuyChosen();
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                cursor.ExitChosen();
                SetState(STATES.Exit);
                cursor.ResetTimer();

            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                cursor.ExitChosen();
                SetState(STATES.Exit);
                cursor.ResetTimer();

            }

            else if (Input.GetKeyDown(KeyCode.Z))
            {
                ItemSelect.SetActive(true); //enables a new window for interaction
                MenuInteractable.SetActive(false); //Disables the text window
                ShopkeeperTextBox.SetActive(false);
                ShopkeeperTextBox2.SetActive(true);
                ShopkeeperTextBox5.SetActive(false);
                ShopkeeperTextBox4.SetActive(false);
                cursor.HealChosen();
                SetState(STATES.Heal);
                cursor.ResetTimer();
            }
        }
    }

    void ExitState()
    {
        if (cursor.TimerCap <= cursor.TimerTracked)
        {
            cursor.ExitChosen();
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                cursor.BuyChosen();
                SetState(STATES.Buy);
                cursor.ResetTimer();

            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                cursor.BuyChosen();
                SetState(STATES.Buy);
                cursor.ResetTimer();

            }

            else if (Input.GetKeyDown(KeyCode.Z))
            {
                SceneManager.LoadScene("L_Corneria");

            }

        }
    }
    void HealState()
    {
        if (cursor.TimerCap <= cursor.TimerTracked)
        {
            cursor.HealChosen();
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                cursor.PureChosen();
                SetState(STATES.Pure);
                cursor.ResetTimer();
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                cursor.TentChosen();
                SetState(STATES.Tent);
                cursor.ResetTimer();
            }
            else if (Input.GetKeyDown(KeyCode.Z))
            {
                cursor.BuyYesChosen();
                SetState(STATES.BuyYes);
                cursor.ResetTimer();
                ShopkeeperTextBox3.SetActive(true);
                ShopkeeperTextBox2.SetActive(false);
                BuyYN.SetActive(true);
            }
            else if (Input.GetKeyDown(KeyCode.X))
            {
                cursor.BuyChosen();
                SetState(STATES.Buy);
                cursor.ResetTimer();
                MenuInteractable.SetActive(true);
                ItemSelect.SetActive(false);
                ShopkeeperTextBox.SetActive(true);
                ShopkeeperTextBox2.SetActive(false);
            }
        }

    }
    void PureState()
    {
        if (cursor.TimerCap <= cursor.TimerTracked)
        {
            cursor.PureChosen();
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                cursor.TentChosen();
                SetState(STATES.Tent);
                cursor.ResetTimer();
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                cursor.HealChosen();
                SetState(STATES.Heal);
                cursor.ResetTimer();
            }
            else if (Input.GetKeyDown(KeyCode.Z))
            {

            }
            else if (Input.GetKeyDown(KeyCode.X))
            {
                cursor.BuyChosen();
                SetState(STATES.Buy);
                cursor.ResetTimer();
                MenuInteractable.SetActive(true);
                ItemSelect.SetActive(false);
                ShopkeeperTextBox.SetActive(true);
                ShopkeeperTextBox2.SetActive(false);
            }
        }
    }
    void TentState()
    {
        if (cursor.TimerCap <= cursor.TimerTracked)
        {
            cursor.TentChosen();
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                cursor.HealChosen();
                SetState(STATES.Heal);
                cursor.ResetTimer();
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                cursor.PureChosen();
                SetState(STATES.Pure);
                cursor.ResetTimer();
            }
            else if (Input.GetKeyDown(KeyCode.Z))
            {

            }
            else if (Input.GetKeyDown(KeyCode.X))
            {
                cursor.BuyChosen();
                SetState(STATES.Buy);
                cursor.ResetTimer();
                MenuInteractable.SetActive(true);
                ItemSelect.SetActive(false);
                ShopkeeperTextBox.SetActive(true);
                ShopkeeperTextBox2.SetActive(false);
            }
        }
    }
    void BuyYesState()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            cursor.BuyNoChosen();
            SetState(STATES.BuyNo);
            cursor.ResetTimer();
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            cursor.BuyNoChosen();
            SetState(STATES.BuyNo);
            cursor.ResetTimer();
        }
        else if (Input.GetKeyDown(KeyCode.Z))
        {
            HealObtained = true;
            ShopkeeperTextBox4.SetActive(true);
            ShopkeeperTextBox3.SetActive(false);
            cursor.BuyChosen();
            SetState(STATES.Buy);
            cursor.ResetTimer();
            BuyYN.SetActive(false);
            MenuInteractable.SetActive(true);
        }
        else if (Input.GetKeyDown(KeyCode.X))
        {
            ShopkeeperTextBox5.SetActive(true);
            ShopkeeperTextBox3.SetActive(false);
            cursor.BuyChosen();
            SetState(STATES.Buy);
            cursor.ResetTimer();
            BuyYN.SetActive(false);
            MenuInteractable.SetActive(true);
        }
    }
    void BuyNoState()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            cursor.BuyYesChosen();
            SetState(STATES.BuyYes);
            cursor.ResetTimer();
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            cursor.BuyYesChosen();
            SetState(STATES.BuyYes);
            cursor.ResetTimer();
        }
        else if (Input.GetKeyDown(KeyCode.Z))
        {
            ShopkeeperTextBox5.SetActive(true);
            ShopkeeperTextBox3.SetActive(false);
            cursor.BuyChosen();
            SetState(STATES.Buy);
            cursor.ResetTimer();
            BuyYN.SetActive(false);
            MenuInteractable.SetActive(true);
        }
        else if (Input.GetKeyDown(KeyCode.X))
        {
            ShopkeeperTextBox5.SetActive(true);
            ShopkeeperTextBox3.SetActive(false);
            cursor.BuyChosen();
            SetState(STATES.Buy);
            cursor.ResetTimer();
            BuyYN.SetActive(false);
            MenuInteractable.SetActive(true);
        }
    }
}
