﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

/* Code by Mike Looney
 Code is meant to control the position of the cursor while in the shop scene*/

public class L_ShopCursor : MonoBehaviour {


    [SerializeField]
    private Transform PM_C;
    [SerializeField]
    private float TimerCapHit;
    private float TimerTracking;

    // Use this for initialization
    void Start()
    { //calls the transform and sets 
        PM_C.GetComponent<Transform>();
        ResetTimer();
    }

    // Update is called once per frame
    void Update()
    {
        TimerTracking += Time.deltaTime;
    }
    //values for the transform of the cursor inside the pause menu
    //also a way to reset the timer to keep from having stack overflow errors
    public void ResetTimer()
    {
        TimerTracking = 0;
    }
    public void BuyChosen()
    {
        PM_C.position = new Vector2(-6.6f, -2.6f);
    }
    public void ExitChosen()
    {
        PM_C.position = new Vector2(-6.6f, -3.25f);
    }
    public void HealChosen()
    {
        PM_C.position = new Vector2(4.5f, 3.8f);
    }
    public void PureChosen()
    {
        PM_C.position = new Vector2(4.5f, 2.9f);
    }
    public void TentChosen()
    {
        PM_C.position = new Vector2(4.5f, 2.1f);
    }
    public void BuyYesChosen()
    {
        PM_C.position = new Vector2(-6.6f, -2.6f);
    }
    public void BuyNoChosen()
    {
        PM_C.position = new Vector2(-6.6f, -3.25f);
    }

    public float TimerCap
    {
        get
        {
            return TimerCapHit;
        }
    }

    public float TimerTracked
    {
        get
        {
            return TimerTracking;
        }
    }
}
