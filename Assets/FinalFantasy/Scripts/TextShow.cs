﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextShow : MonoBehaviour
{

    //Code By: Rahul Yerramneedi
    //Feature:  This script displays the text boxes for the npc's and also freezes the game when you speak to them and resumes it when you stop the interaction.
    //Additional Notes: For now, it displays the images of the text. Further down, you can change the "Image" to an U.I based system.

    public bool isImgOn;
    public Image img;

    void Start()
    {

        img.enabled = false;
        isImgOn = false;
    }

    void Update()
    {



        if (Input.GetKeyDown(KeyCode.O))
        {
            if(isImgOn == true)
            {
                img.enabled = false;
                isImgOn = false;
                Time.timeScale = 1;
            }
        }
    }


    void OnTriggerStay2D(Collider2D collision)
    {
        if (Input.GetKeyDown(KeyCode.P))
        {

            if (isImgOn == false)
            {

                img.enabled = true;
                isImgOn = true;
                Time.timeScale = 0;
                
            }

            //else
            //{

            //    img.enabled = false;
            //    isImgOn = false;
            //}
        }

        //else if (Input.GetKeyDown(KeyCode.O))
        //{
        //    if (isImgOn == true)
        //    {
        //        img.enabled = false;
        //        isImgOn = false;
        //        Time.timeScale = 1;
        //    }
        //}
    }
}
