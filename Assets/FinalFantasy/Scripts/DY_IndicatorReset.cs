﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DY_IndicatorReset : MonoBehaviour
{

    TextMesh text;

    private void OnDisable()
    {
        text.text = "";
    }

    // Use this for initialization
    void Start ()
    {
        text = GetComponent<TextMesh>();
        text.text = "";
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
