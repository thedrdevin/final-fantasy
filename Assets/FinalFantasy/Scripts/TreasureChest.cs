﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureChest : MonoBehaviour {

    //Code By: Rahul Yerramneedi
    //Feature: This script is used to open the treasure chest.
    //Edited By: Divya Patel
    //Feature: Chest now gets destroyed after use.


    public Animator myanim;

	// Use this for initialization
	void Start () {
        myanim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update(){
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            myanim.SetTrigger("OpenMe");
            SScript.scorevalue += 20;
            Destroy(gameObject);
        }
        else if (Input.GetKeyDown(KeyCode.O))
        {
            myanim.SetTrigger("CloseMe");
        }
    }
}
