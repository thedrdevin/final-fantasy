﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCMOV : MonoBehaviour {

    //Code By: Rahul Yerramneedi

    public float movementspeed;
    public float walkTime;
    public float waitTime;

    private Vector2 minWalkPoint;
    private Vector2 maxWalkPoint;

    private float walkcounter;
    private float waitcounter;
    private Rigidbody2D myRB;
    public bool isWalk;

    private int walkDirection;
    public Collider2D WalKZone;

    public bool haswalkzone;

	// Use this for initialization
	void Start ()
    {
        myRB = GetComponent<Rigidbody2D>();
        waitcounter = waitTime;
        walkcounter = walkTime;
        ChooseDirection();

        if(WalKZone != null)
        {
            minWalkPoint = WalKZone.bounds.min;
            maxWalkPoint = WalKZone.bounds.max;
            haswalkzone = true;
        }  
    }
	
	// Update is called once per frame
	void Update () {
        if (isWalk)
        {
            walkcounter -= Time.deltaTime;
            if(walkcounter < 0)
            {
                isWalk = false;
                waitcounter = waitTime;
            }

            switch (walkDirection)
            {
                case 0:
                    myRB.velocity = new Vector2(0, movementspeed);
                    if(haswalkzone && transform.position.y > maxWalkPoint.y)
                    {
                        waitcounter = waitTime;
                    }
                    break;

                case 1:
                    myRB.velocity = new Vector2(movementspeed, 0);
                    if (haswalkzone && transform.position.x > maxWalkPoint.x)
                    {
                        waitcounter = waitTime;
                    }
                    break;

                case 2:
                    myRB.velocity = new Vector2(0, -movementspeed);
                    if (haswalkzone && transform.position.y < minWalkPoint.y)
                    {
                        waitcounter = waitTime;
                    }
                    break;

                case 3:
                    myRB.velocity = new Vector2(-movementspeed, 0);
                    if (haswalkzone && transform.position.x < minWalkPoint.x)
                    {
                        waitcounter = waitTime;
                    }
                    break;
            }

            if (walkcounter < 0)
            {
                isWalk = false;
                waitcounter = waitTime;
            }

        }
        else
        {
            waitcounter -= Time.deltaTime;
            myRB.velocity = Vector2.zero;

            if(waitcounter < 0)
            {
                ChooseDirection();
            }
        }
	}

    public void ChooseDirection()
    {
        walkDirection = Random.Range(0, 4);
        isWalk = true;
        walkcounter = walkTime;
    }

}
