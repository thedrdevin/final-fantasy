﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingInteract : MonoBehaviour {
    [SerializeField] GameObject roof;

    bool ran = false;  
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // If player enters building box collider && presses 'E' the roof disappears
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && Input.GetKeyDown(KeyCode.E) && !ran)
        {
            roof.SetActive(false);
            ran = true;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            roof.SetActive(false);
        }
    }
    // if the player leaves the building, the roof reappears.
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" && Input.GetKeyDown(KeyCode.E) && ran)
        {
            roof.SetActive(true);
            ran = false;
        }
    }
}
