﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CIMenu : MonoBehaviour {

    public GameObject PauseUI;

    private bool paused = false;

	// Use this for initialization
	void Start ()
    {
        PauseUI.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            paused = !paused;
        }

        if (!paused)
        {
            PauseUI.SetActive(false);
            Time.timeScale = 1;
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (paused)
        {
            PauseUI.SetActive(true);
            Time.timeScale = 0;
        }
    }
}
