﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//This handles the temperary buttons in the main menu

public class ButtonManager : MonoBehaviour {

    public void newGameBtn(string newGameLevel)
    {
        Debug.Log("LoadScene");
        SceneManager.LoadScene(newGameLevel);
    }

}
