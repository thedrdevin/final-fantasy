﻿/*-------------------------------------------------------------------------------------------------------------------------------
* Name: Devin Rhoads
 * Date: 5/9/2018
 * Credit: Project : FinalFantasy 1 Nes Remake
 * Purpose: This script handles the movement of the main menu cursor and loads the game overworld when the player presses enter.
 * 
 --------------------------------------------------------------------------------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class DR_MMCursor : MonoBehaviour {

    public GameObject cursor1;
    public GameObject cursor2;
    public bool canClick;
    private float restTime;

    void Start ()
    {
        canClick = true;
        restTime = 0.5f;
	}
	
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            if (cursor1.activeInHierarchy == true && canClick == true)
            {
                Debug.Log("2 now active");
                cursor1.SetActive(false);
                cursor2.SetActive(true);
                canClick = false;
                StartCoroutine(rest());
            }
            if (cursor2.activeInHierarchy == true && canClick == true)
            {
                Debug.Log("1 now active");

                cursor2.SetActive(false);
                cursor1.SetActive(true);
                canClick = false;
                StartCoroutine(rest());
            }
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            if (cursor1.activeInHierarchy == true && canClick == true)
            {
                Debug.Log("2 now active");
                cursor1.SetActive(false);
                cursor2.SetActive(true);
                canClick = false;
                StartCoroutine(rest());
            }
            if (cursor2.activeInHierarchy == true && canClick == true)
            {
                Debug.Log("1 now active");
                cursor2.SetActive(false);
                cursor1.SetActive(true);
                canClick = false;
                StartCoroutine(rest());
            }
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (cursor1.activeInHierarchy == true)
            {
                SceneManager.LoadScene("FinalizedAlpha");

            }
            if (cursor2.activeInHierarchy == true)
            {
                SceneManager.LoadScene("FinalizedAlpha");

            }
        }
    }

    IEnumerator rest ()
    {
        yield return new WaitForSeconds(restTime);
        canClick = true;
    }
}
